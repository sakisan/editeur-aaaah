package interactivity
{
	import flash.display.Sprite;
	import flash.events.*;
	import flash.net.XMLSocket;
	import flash.system.Security;
	import flash.text.TextField;
	
	import properties.Getter;
	import properties.Properties;
	
	public class Communication extends Sprite {
		private var hostName:String = Properties.server;
		private var port:uint = Properties.port;
		private var socket:XMLSocket;
		
		;
		private var multi:String = "";
		private var revision:Number = new Number(0)
		private var myLastCommit:String = "";	
		//public var test:TextField;
		
		public function Communication() {
			Security.loadPolicyFile("http://www.extinction.fr/crossdomain.xml");
			Security.allowDomain("*");
			
			socket = new XMLSocket(hostName, port);
			configureListeners(socket);
			//socket.connect(hostName, port);
			
		//	test = new TextField();
		//	test.x = 300;
		//	test.y = 450;
		//	test.width = 300;
		//	test.height = 500;
		//	test.wordWrap =  true;
			//test.text = "Constructor : No Problem\n";
		//	addChild(test);
		}
		
		public function send(data:Object):void {
//			test.appendText("Sending "+data.toString()+"\n");
			socket.send(data);
		}
		
		private function configureListeners(dispatcher:IEventDispatcher):void {
			dispatcher.addEventListener(Event.CLOSE, closeHandler);
			dispatcher.addEventListener(Event.CONNECT, connectHandler);
			dispatcher.addEventListener(DataEvent.DATA, dataHandler);
			dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		}
		
		private function closeHandler(event:Event):void {
		//	test.appendText( "closeHandler: " + event +"\n");
		}
		
		private function connectHandler(event:Event):void {
		//	test.appendText( "connectHandler: " + event+"\n");
		}
		
		private function dataHandler(event:DataEvent):void {
		//	test.appendText( "dataHandler: " + event.data+"\n");
			
			var info:Array = event.data.split("#");
			var infoRevision:Number = new Number(info[0]);
			if(infoRevision > revision){
				revision = infoRevision;
				Getter.editer.loadCode(info[1]);
			}
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void {
		//	test.appendText( "ioErrorHandler: " + event+"\n");
		}
		
		private function progressHandler(event:ProgressEvent):void {
		//	test.appendText( "progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal+"\n");
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void {
		//	test.appendText( "securityErrorHandler: " + event+"\n");
		}
		
		public function joinMulti(name:String):void
		{
			if(this.multi != name)
			{
				this.multi = name;
				send("Salon#"+name);
				revision = 0;
			}
		}
		
		public function commitRevision(code:String)
		{
			if(code != myLastCommit ){
				revision++;
				myLastCommit = code;
				send("Code#"+revision+"#"+code);
			}
		}
	}
}