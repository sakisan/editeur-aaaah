package drawing
{      
	import blocks.Rectangle;
	
	import editeur.Drawboard;
	
	import flash.display.CapsStyle;
	import flash.display.Graphics;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import properties.Getter;
	import properties.State;
	
	public class RectangleDrawer extends ShapeDrawer
	{
		protected var rectangle:Rectangle;
		
		public function RectangleDrawer(d:Drawboard)
		{
			super(d);
			state = "Dessin";
		}
		
		override public function isRealShapeDrawer():Boolean
		{
			return true;
		}
		
		override public function clickHandler(e:MouseEvent):void
		{
			if(teller == 0){
				rectangle = new Rectangle(drawer);
				g =  rectangle.newGraphics.getGraphics();
				
				teller = 1;
				rectangle.xPosition = Getter.stageToDrawfieldX(e.stageX);
				rectangle.yPosition = Getter.stageToDrawfieldY(e.stageY);
				g.moveTo(0,0);
			}
			else{
				if(rectangle.myWidth != 0 || rectangle.myHeight != 0){
					teller = 0;
					if(State.full){
						g.beginFill(rectangle.newGraphics.color);
						rectangle.full = 1;
					}
					else rectangle.full = 0;
					g.drawRect(0,0,rectangle.myWidth,rectangle.myHeight);
					g.endFill();
					drawer.addElement(rectangle);
				}
			}
		}
		
		override public function moveHandler(e:MouseEvent):void
		{
			if(teller == 1){
				graphics.clear();
				graphics.lineStyle(rectangle.thickness,0x000000, 1, true, LineScaleMode.NORMAL, null,JointStyle.MITER,3);
				if(State.full)
					graphics.beginFill(rectangle.newGraphics.color);
				if(!e.shiftKey){
					rectangle.myWidth = Getter.stageToDrawfieldX(e.stageX)-rectangle.xPosition;
					rectangle.myHeight = Getter.stageToDrawfieldY(e.stageY)-rectangle.yPosition;
				}
				else{
					if(Math.abs(Getter.stageToDrawfieldX(e.stageX) - rectangle.xPosition) > Math.abs(Getter.stageToDrawfieldY(e.stageY) - rectangle.yPosition)){
						rectangle.myWidth = Getter.stageToDrawfieldX(e.stageX)-rectangle.xPosition;
						if((Getter.stageToDrawfieldX(e.stageX)-rectangle.xPosition)*(Getter.stageToDrawfieldY(e.stageY)-rectangle.yPosition) > 0)
							rectangle.myHeight = rectangle.myWidth;
						else rectangle.myHeight = - rectangle.myWidth;
					}
					else {
						rectangle.myHeight = Getter.stageToDrawfieldY(e.stageY)-rectangle.yPosition;
						if((Getter.stageToDrawfieldX(e.stageX)-rectangle.xPosition)*(Getter.stageToDrawfieldY(e.stageY)-rectangle.yPosition) > 0)
							rectangle.myWidth = rectangle.myHeight;
						else rectangle.myWidth = - rectangle.myHeight;
					}
				}
				graphics.drawRect(rectangle.xPosition,rectangle.yPosition,rectangle.myWidth,rectangle.myHeight);
			}
		}

	}
}
