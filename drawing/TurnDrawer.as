package drawing
{      
	import blocks.Element;
	import blocks.Line;
	
	import editeur.Drawboard;
	
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	
	import properties.Getter;
	import properties.Properties;
	import properties.TimeTraveller;
	
	public class TurnDrawer extends ShapeDrawer{
		
		private var line:Line;
		private var angle:Number = 0;
		private var element:Element = Element(Getter.editer.mySelection.shapes[0]);
		private var centerX:Number;
		private var centerY:Number;
		
		public function TurnDrawer(d:Drawboard)
		{
			super(d);
			state = "Tournage";
		}
		
		override public function isRealShapeDrawer():Boolean
		{
			return true;
		}
		
		override public function clickHandler(e:MouseEvent):void
		{
			if(teller == 0){
				line = new Line(drawer);
				line.changeColorTo(Properties.turnButtonColor);
				g = line.newGraphics.getGraphics();
				
				teller = 1;
				line.xPosition = Getter.stageToDrawfieldX(e.stageX);
				line.yPosition = Getter.stageToDrawfieldY(e.stageY);
				g.moveTo(0,0);
			}
			else{
				teller = 0;
				if(angle != 0){
					element.turn(angle, centerX, centerY);
					var animate:Matrix;
					animate = new Matrix();
					animate.translate(-centerX,-centerY);
					animate.rotate( 0);
					animate.translate(centerX,centerY);
					element.transform.matrix = animate;
	//				element.rotation = 0;
				}
			}
		}
		
		override public function moveHandler(e:MouseEvent):void
		{
			if(teller == 1){
				graphics.clear();
				graphics.lineStyle(line.thickness, line.newGraphics.color);
				graphics.moveTo(line.xPosition,line.yPosition);
				graphics.lineTo(Getter.stageToDrawfieldX(e.stageX), Getter.stageToDrawfieldY(e.stageY));
				
				line.xDirection = Getter.stageToDrawfieldX(e.stageX) - line.xPosition;
				line.yDirection = Getter.stageToDrawfieldY(e.stageY) - line.yPosition;
			}
			
			//turn the selection
			if(e.shiftKey){
				centerX = line.xPosition;
				centerY = line.yPosition;
			}
			else{
				centerX = element.rotationCenterX;
				centerY = element.rotationCenterY;
			}
			
			if(teller > 0){
				angle = Math.atan(line.yDirection/line.xDirection);
				if(line.xDirection <0 )
					angle = Math.PI + angle;
				
				var animate:Matrix;
				animate = new Matrix();
				animate.translate(-centerX,-centerY);
				animate.rotate(angle);
				animate.translate(centerX,centerY);
				element.transform.matrix = animate;
			}
		}
	}
}
