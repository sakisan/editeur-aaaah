package drawing
{      
	import blocks.Line;
import blocks.Polygon;

import editeur.Drawboard;
	
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import properties.Getter;
	
	public class PolygonDrawer extends ShapeDrawer{
		
		private var line:Line;
		private var polygon:Polygon;

		public function PolygonDrawer(d:Drawboard)
		{
			super(d);
			state = "Dessin";
		}
		
		override public function isRealShapeDrawer():Boolean
		{
			return true;
		}
		
		override public function clickHandler(e:MouseEvent):void
		{
			if(teller == 0){
				polygon = new Polygon(drawer);
				drawer.addElement(polygon);

				line = new Line(drawer);
				g = line.newGraphics.getGraphics();

				line.xPosition = Getter.stageToDrawfieldX(e.stageX);
				line.yPosition = Getter.stageToDrawfieldY(e.stageY);
				polygon.xPosition = Getter.stageToDrawfieldX(e.stageX);
				polygon.yPosition = Getter.stageToDrawfieldY(e.stageY);
				g.moveTo(0,0);
				teller = 1;

			}
			else{
				if( line.xDirection != 0 || line.yDirection !=0){
					g.lineTo(line.xDirection,line.yDirection);
					polygon.addLine(line);


					line = new Line(drawer);
					g = line.newGraphics.getGraphics();

					line.xPosition = Getter.stageToDrawfieldX(e.stageX);
					line.yPosition = Getter.stageToDrawfieldY(e.stageY);
					g.moveTo(0,0);

				}
			}
			moveHandler(e);
		}
		
		override public function moveHandler(e:MouseEvent):void
		{
			if(teller == 1){
				graphics.clear();
				graphics.lineStyle(line.thickness, line.newGraphics.color);
				graphics.moveTo(line.xPosition,line.yPosition);
				if(!e.shiftKey){
					graphics.lineTo(Getter.stageToDrawfieldX(e.stageX), Getter.stageToDrawfieldY(e.stageY));
					
					line.xDirection = Getter.stageToDrawfieldX(e.stageX) - line.xPosition;
					line.yDirection = Getter.stageToDrawfieldY(e.stageY) - line.yPosition;
				}
				else{ 
					if(Math.abs(Getter.stageToDrawfieldX(e.stageX) - line.xPosition) > Math.abs(Getter.stageToDrawfieldY(e.stageY) - line.yPosition)){
						graphics.lineTo(Getter.stageToDrawfieldX(e.stageX), line.yPosition);
						
						line.xDirection = Getter.stageToDrawfieldX(e.stageX) - line.xPosition;
						line.yDirection = 0;
					}
					else{
						graphics.lineTo(line.xPosition,Getter.stageToDrawfieldY(e.stageY));
						line.xDirection = 0;
						line.yDirection = Getter.stageToDrawfieldY(e.stageY) - line.yPosition;
					}
				}
				polygon.fictiveLine = line;
				polygon.redraw();
			}
		}

		override public function terminate():void {
			polygon.roundUp();
			if(polygon.lines.shapes.length < 3 ){
				drawer.eraseShape(polygon, true);
			}
			teller = 0;
		}
	}

}
