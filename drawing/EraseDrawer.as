package drawing
{
	import editeur.Drawboard;

	public class EraseDrawer extends ShapeDrawer
	{
		public function EraseDrawer(d:Drawboard)
		{
			super(d);
			state = "Gomme";
		}
	}
}