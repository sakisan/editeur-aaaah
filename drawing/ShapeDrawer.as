package drawing
{
	import editeur.Drawboard;
	
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import support.NewGraphics;

	public class ShapeDrawer extends MovieClip
	{
		private var _teller:int;
		private var _g:Graphics;
		private var _newgraphics:NewGraphics;
		private var _drawer:Drawboard;
		private var _state:String;
		
		public function ShapeDrawer(d:Drawboard)
		{
			drawer = d;
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		public function clickHandler(e:MouseEvent):void
		{
		}
		
		public function moveHandler(e:MouseEvent):void
		{
		}
		
		private function update(e:Event):void
		{
			if(teller == 0)
				graphics.clear();
		}
		
		public function drawing():Boolean
		{
			return teller != 0;
		}
		
		public function isRealShapeDrawer():Boolean
		{
			return false;
		}
		
		// GETTERS & SETTERS
		
		public function get teller():int
		{
			return _teller;
		}

		public function set teller(value:int):void
		{
			_teller = value;
		}

		public function get g():Graphics
		{
			return _g;
		}

		public function set g(value:Graphics):void
		{
			_g = value;
		}

		public function get newgraphics():NewGraphics
		{
			return _newgraphics;
		}

		public function set newgraphics(value:NewGraphics):void
		{
			_newgraphics = value;
		}

		protected function get drawer():Drawboard
		{
			return _drawer;
		}

		protected function set drawer(value:Drawboard):void
		{
			_drawer = value;
		}
		
		public function get state():String
		{
			return _state;
		}

		public function set state(value:String):void
		{
			_state = value;
		}

		public function terminate():void {

		}
	}
}