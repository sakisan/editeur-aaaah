package drawing
{
	import blocks.Element;
	import blocks.Group;
	import blocks.Rectangle;
	import blocks.Shape;
	
	import editeur.Drawboard;
	
	import flash.display.CapsStyle;
	import flash.display.DisplayObject;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.events.MouseEvent;
	
	import properties.Getter;
	import properties.Properties;
	
	import support.ElementsIterator;
	import support.GroupIterator;
	
	public class SelectionRectangleDrawer extends RectangleDrawer
	{
		
		public function SelectionRectangleDrawer(d:Drawboard = null)
		{
			super(null);
			state = "Selection";
		}
		
		override public function isRealShapeDrawer():Boolean
		{
			return false;
		}
		
		override public function clickHandler(e:MouseEvent):void
		{
			if(Getter.drawboard.contains(DisplayObject(e.target)))
			{
					if(teller == 0){
					rectangle = new Rectangle(drawer);
					rectangle.thickness = 1;
					g =  rectangle.newGraphics.getGraphics();
					
					teller = 1;
					rectangle.xPosition = Getter.stageToDrawfieldX(e.stageX);
					rectangle.yPosition = Getter.stageToDrawfieldY(e.stageY);
					g.moveTo(Getter.stageToDrawfieldX(e.stageX),Getter.stageToDrawfieldY(e.stageY));
				}
				else{
					teller = 0;
					g.beginFill(Properties.selectionButtonColor, 0.5);
					rectangle.full = 1;
					g.drawRect(rectangle.xPosition,rectangle.yPosition,rectangle.myWidth,rectangle.myHeight);
					g.endFill();
					this.addChild(rectangle);
					//add shapes to selection
					var it:ElementsIterator = Getter.drawboard.iterator;
					while(it.hasNext())
					{
						var element:Element = it.next();
						if (element is Group)
						{
							var git:GroupIterator = Group(element).iterator;
							while(git.hasNext())
							{
								var shape:Shape = git.next();
//								if (shape.newGraphics.hitTestObject(rectangle.newGraphics))
//									Getter.editer.addToSelection(shape);
								if(hitTest(shape))
									Getter.editer.addToSelection(shape);
							}
						}
						else if(element is Shape){
//							if (Shape(element).newGraphics.hitTestObject(rectangle.newGraphics)) 
//								Getter.editer.addToSelection(Shape(element));
							if(hitTest(Shape(element)))
								Getter.editer.addToSelection(Shape(element));
						}
					}
					this.removeChild(rectangle);
				}
			}
		}
		
		override public function moveHandler(e:MouseEvent):void
		{
			if(teller == 1){
				graphics.clear();
				graphics.lineStyle(rectangle.thickness,Properties.selectionButtonColor, 1, false, LineScaleMode.NORMAL, CapsStyle.SQUARE,JointStyle.MITER);
				graphics.beginFill(Properties.selectionButtonColor, 0.5);
				if(!e.shiftKey){
					rectangle.myWidth = Getter.stageToDrawfieldX(e.stageX)-rectangle.xPosition;
					rectangle.myHeight = Getter.stageToDrawfieldY(e.stageY)-rectangle.yPosition;
				}
				else{
					if(Math.abs(Getter.stageToDrawfieldX(e.stageX) - rectangle.xPosition) > Math.abs(Getter.stageToDrawfieldY(e.stageY) - rectangle.yPosition)){
						rectangle.myWidth = Getter.stageToDrawfieldX(e.stageX)-rectangle.xPosition;
						if((Getter.stageToDrawfieldX(e.stageX)-rectangle.xPosition)*(Getter.stageToDrawfieldY(e.stageY)-rectangle.yPosition) > 0)
							rectangle.myHeight = rectangle.myWidth;
						else rectangle.myHeight = - rectangle.myWidth;
					}
					else {
						rectangle.myHeight = Getter.stageToDrawfieldY(e.stageY)-rectangle.yPosition;
						if((Getter.stageToDrawfieldX(e.stageX)-rectangle.xPosition)*(Getter.stageToDrawfieldY(e.stageY)-rectangle.yPosition) > 0)
							rectangle.myWidth = rectangle.myHeight;
						else rectangle.myWidth = - rectangle.myHeight;
					}
				}
				graphics.drawRect(rectangle.xPosition,rectangle.yPosition,rectangle.myWidth,rectangle.myHeight);
			}
		}
		
		private function hitTest(shape:Shape):Boolean
		{
			var result:Boolean = false;
			var startI:int; 
			var endI:int;
			var startJ:int;
			var endJ:int;
			
			if(rectangle.myWidth < 0){
				startI = rectangle.xPosition + rectangle.myWidth;
				endI = rectangle.xPosition;
			}
			else{
				startI = rectangle.xPosition;
				endI = rectangle.xPosition + rectangle.myWidth;
			}
			
			if(rectangle.myHeight < 0){
				startJ = rectangle.yPosition + rectangle.myHeight;
				endJ = rectangle.yPosition;
			}
			else{
				startJ = rectangle.yPosition;
				endJ = rectangle.yPosition + rectangle.myHeight;
			}	
			
			if(shape.rotationCenterX - shape.newGraphics.width/2 > startI)
				startI = shape.rotationCenterX - shape.newGraphics.width/2;
			if(shape.rotationCenterX + shape.newGraphics.width/2 < endI)
				endI = Math.ceil(shape.rotationCenterX + shape.newGraphics.width/2);
			if(shape.rotationCenterY - shape.newGraphics.height/2 > startJ)
				startJ = shape.rotationCenterY - shape.newGraphics.height/2;
			if(shape.rotationCenterY + shape.newGraphics.height/2 < endJ)
				endJ = Math.ceil(shape.rotationCenterY + shape.newGraphics.height/2);
			
			for(var i:Number = startI; i <= endI; i+=1/*Math.ceil(shape.thickness*Getter.drawboard.scaleX/2)*/)
			{
				for(var j:Number = startJ ; j <= endJ; j+=1/*Math.ceil(shape.thickness*Getter.drawboard.scaleY/2)*/)
				{
					result = result || shape.newGraphics.hitTestPoint(i,j,true);
				}
			}
			
			return result;
		}
	}
}