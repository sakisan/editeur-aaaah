package drawing
{      
	import blocks.Ellipse;
	
	import editeur.Drawboard;
	
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import properties.Getter;
	import properties.State;
	
	public class EllipseDrawer extends ShapeDrawer
	{
		private var ellipse:Ellipse;
		
		public function EllipseDrawer(d:Drawboard)
		{
			super(d);
			state = "Dessin";
		}
		
		override public function isRealShapeDrawer():Boolean
		{
			return true;
		}
		
		override public function clickHandler(e:MouseEvent):void
		{
			if(teller == 0){
				ellipse = new Ellipse(drawer);
				g = ellipse.newGraphics.getGraphics();
				
				teller = 1;
				ellipse.xPosition = Getter.stageToDrawfieldX(e.stageX);
				ellipse.yPosition = Getter.stageToDrawfieldY(e.stageY);
				g.moveTo(0,0);
			}
			else{
				if(ellipse.myWidth != 0 || ellipse.myHeight != 0){
					teller = 0;
					if(State.full){
						g.beginFill(ellipse.newGraphics.color);
						ellipse.full = 1;
					}
					else ellipse.full = 0;
					g.drawEllipse(0,0,ellipse.myWidth,ellipse.myHeight);
					g.endFill();
					drawer.addElement(ellipse);
				}
			}
		}
		
		override public function moveHandler(e:MouseEvent):void
		{
			if(teller == 1){
				graphics.clear();
				graphics.lineStyle(ellipse.thickness,ellipse.newGraphics.color);
				if(State.full)
					graphics.beginFill(ellipse.newGraphics.color);
				if(!e.shiftKey){
					ellipse.myWidth = Getter.stageToDrawfieldX(e.stageX)-ellipse.xPosition;
					ellipse.myHeight = Getter.stageToDrawfieldY(e.stageY)-ellipse.yPosition;
				}
				else{
					if(Math.abs(Getter.stageToDrawfieldX(e.stageX) - ellipse.xPosition) > Math.abs(Getter.stageToDrawfieldY(e.stageY) - ellipse.yPosition)){
						ellipse.myWidth = Getter.stageToDrawfieldX(e.stageX)-ellipse.xPosition;
						if((Getter.stageToDrawfieldX(e.stageX)-ellipse.xPosition)*(Getter.stageToDrawfieldY(e.stageY)-ellipse.yPosition) > 0)
							ellipse.myHeight = ellipse.myWidth;
						else ellipse.myHeight = - ellipse.myWidth;
					}
					else {
						ellipse.myHeight = Getter.stageToDrawfieldY(e.stageY)-ellipse.yPosition;
						if((Getter.stageToDrawfieldX(e.stageX)-ellipse.xPosition)*(Getter.stageToDrawfieldY(e.stageY)-ellipse.yPosition) > 0)
							ellipse.myWidth = ellipse.myHeight;
						else ellipse.myWidth = - ellipse.myHeight;
					}
				}
				graphics.drawEllipse(ellipse.xPosition,ellipse.yPosition,ellipse.myWidth,ellipse.myHeight);
			}
		}	
	}
}
