package drawing
{
	import editeur.Drawboard;

	public class NullDrawer extends ShapeDrawer
	{
		public function NullDrawer(d:Drawboard, state:String)
		{
			super(d);
			this.state = state;
		}
	}
}