package drawing
{
	import editeur.Drawboard;
	
	import properties.Getter;

	public class ShapeDrawerFactory
	{		
		public static function createDrawer(msg:String, state:String = null):ShapeDrawer
		{
			var drawer:ShapeDrawer = null;
			var drawboard:Drawboard = Getter.drawboard;
			
			// CHOOSE DRAWERSTRATEGY
			if(msg == "Ligne")
				 drawer = new LineDrawer(drawboard);
			else if(msg == "Rectangle")
				drawer = new RectangleDrawer(drawboard);
			else if(msg == "Ellipse")
				drawer = new EllipseDrawer(drawboard);
			else if(msg == "Courbe") 
				 drawer = new CurveDrawer(drawboard);
			else if(msg == "Gomme")
				drawer = new EraseDrawer(drawboard);
			else if (msg == "SelectionRectangle")
				drawer = new SelectionRectangleDrawer();
			else if (msg == "Tournage")
				drawer = new TurnDrawer(drawboard);
			else if (msg == "Null")
				drawer =  new NullDrawer(drawboard, state);
			else if (msg == "Polygone")
				drawer =  new PolygonDrawer(drawboard);

			return drawer;			
		}
	}
}