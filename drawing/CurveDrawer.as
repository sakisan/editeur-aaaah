package drawing
{      
	import blocks.Curve;
	
	import editeur.Drawboard;
	
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import properties.Getter;
	
	public class CurveDrawer extends ShapeDrawer{
		
		private var curve:Curve;
		
		public function CurveDrawer(d:Drawboard)
		{
			super(d);
			state = "Dessin";
		}
		
		override public function isRealShapeDrawer():Boolean
		{
			return true;
		}
		
		override public function clickHandler(e:MouseEvent):void
		{
			if(teller == 0){
				curve = new Curve(drawer);
				g = curve.newGraphics.getGraphics();
				
				teller = 1;
				curve.xPosition = Getter.stageToDrawfieldX(e.stageX); 
				curve.yPosition = Getter.stageToDrawfieldY(e.stageY);
				g.moveTo(0,0);
			}
			else if(teller == 1)
			{
				
				curve.xDirection = Getter.stageToDrawfieldX(e.stageX) - curve.xPosition;
				curve.yDirection = Getter.stageToDrawfieldY(e.stageY) - curve.yPosition;
				if(curve.xDirection != 0 || curve.yDirection != 0){
					teller = 2;
					g.lineTo(curve.xDirection,curve.yDirection);
				}
			}
			else {
				teller = 0;
				curve.xBend = Getter.stageToDrawfieldX(e.stageX) - curve.xPosition;
				curve.yBend = Getter.stageToDrawfieldY(e.stageY) - curve.yPosition;
				g.clear();
				g.lineStyle(curve.thickness, curve.newGraphics.color);
				g.moveTo(0, 0);
				g.curveTo(curve.xBend, curve.yBend ,curve.xDirection, curve.yDirection);
				drawer.addElement(curve); 
			}
		}
		
		override public function moveHandler(e:MouseEvent):void
		{
			if(teller == 1){
				graphics.clear();
				graphics.lineStyle(curve.thickness, curve.newGraphics.color);
				graphics.moveTo(curve.xPosition,curve.yPosition);
				graphics.lineTo(Getter.stageToDrawfieldX(e.stageX), Getter.stageToDrawfieldY(e.stageY));
			}
			if(teller == 2){
				graphics.clear();
				graphics.lineStyle(curve.thickness, curve.newGraphics.color);
				graphics.moveTo(curve.xPosition, curve.yPosition);
				graphics.curveTo(Getter.stageToDrawfieldX(e.stageX), Getter.stageToDrawfieldY(e.stageY), curve.xDirection+curve.xPosition, curve.yDirection+curve.yPosition);
			}
		}		
	}
}
