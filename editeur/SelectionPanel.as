package editeur
{
	import blocks.Element;
	import blocks.Selection;
	import blocks.Shape;
	
	import flash.display.MovieClip;
	
	import support.MyButton;
	
	public class SelectionPanel extends MovieClip
	{
		private var translationPanel:TranslationPanel;
		private var rotationPanel:RotationPanel;
		private var elementPanel:ElementPanel;
		
		public function SelectionPanel(selection:Selection,x:int, y:int)
		{
			var shape:Shape = selection.shapes[0];
			var element:Element;
			if(shape.group == null )
				element = shape;
			else if(selection.shapes.length == 1)
				element = shape;
			else
				element = shape.group;
			
			elementPanel = new ElementPanel(element, x, y, 120, 20);
			addChild(elementPanel);
			
			translationPanel = new TranslationPanel(element, x+140, y, 100, 20);
			addChild(translationPanel);
			rotationPanel = new RotationPanel(element, x+300, y, 80, 20);
			addChild(rotationPanel);
		}
	}
}