package editeur
{
	import blocks.*;
	
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.text.StyleSheet;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	import properties.Properties;
	import properties.State;
	
	import support.CodeParser;
	import support.MyButton;
	
	public class CodeRenderer extends MovieClip
	{
		private var _codeParser:CodeParser;
		
		private var codeBox:TextField;
		private var clickButton:MyButton;
		private var copyButton:MyButton;
		private var style:StyleSheet;
		
		private  var smallHeight:int = 170;
		private  var bigHeight:int = 570;
		private  var smallWidth:int = 250;
		private  var bigWidth:int = 730;
		private  var smallX:int = 900-smallWidth;
		private  var bigX:int = 30;
		private  var smallY:int = 410;
		private  var bigY:int = 30;
		private  var buttonHeight:int = 20;
		
		public function CodeRenderer(drawer:Drawboard)
		{
			_codeParser = new CodeParser(drawer);
			setupCodeBox();
			codeBox.text = "<C><G/><F/></C>";
			//			codeBox.wordWrap = true;

//						setupStyle();
			
			clickButton = new MyButton(smallX,smallY+smallHeight,smallWidth,buttonHeight,"Click",Properties.codeBackgroundColor,1);
			addChild(clickButton);
			clickButton.addEventListener(MouseEvent.MOUSE_DOWN, switchSize);
			copyButton = new MyButton(smallX,smallY+smallHeight-buttonHeight,smallWidth,buttonHeight,"Copier",Properties.codeBackgroundColor,1);
			addChild(copyButton);
			copyButton.addEventListener(MouseEvent.MOUSE_DOWN, copyCodeToClipboard);
			codeBox.addEventListener(FocusEvent.FOCUS_IN, removeStyle);
			codeBox.addEventListener(FocusEvent.FOCUS_OUT, addStyle);
			
			drawBackground();
		}
		
		private function setupCodeBox():void
		{
			if(codeBox != null)
				removeChild(codeBox);
			
			codeBox = new TextField;
			codeBox.width = smallWidth;
			codeBox.height = smallHeight;
			codeBox.y = smallY;
			codeBox.x = smallX;
			addChild(codeBox);
			codeBox.type = TextFieldType.INPUT;
		}
		
		private function setupStyle():void
		{			
			style = new StyleSheet;
			
			var body:Object = new Object();
			body.fontWeight = "bold";
			style.setStyle("body", body);
			
			var params:Object = new Object();
			params.color = "#003366";
			style.setStyle(".params", params);
			
			var l:Object = new Object();
			l.color = "#0000FF";
			style.setStyle(".L", l);
			
			var r:Object = new Object();
			r.color = "#d50000";
			style.setStyle(".R", r);
			
			var e:Object = new Object();
			e.color = "#8000b5";
			style.setStyle(".E", e);
			
			var c:Object = new Object();
			c.color = "#008700";
			style.setStyle(".C", c);
			
			var g:Object = new Object();
			g.color = "#918800";
//			style.setStyle(".G", g);
			
			styleSheet = style;
			
		}
		
		private function addStyle(e:FocusEvent):void
		{
			styleSheet = style;
		}
		
		private function removeStyle(e:FocusEvent):void
		{
			styleSheet = null;
		}
		
		public function get styleSheet():StyleSheet
		{
			return codeBox.styleSheet;
		}
		
		public function set styleSheet(style:StyleSheet):void
		{
			codeBox.styleSheet = style;
		}
		
		public function updateCode(elements:Array):void
		{
			var text:String = "";
			text += "<C>\r";
			text += "<G>\n";
			text += generateGroupCode(elements);
			text += "</G>\n";
			text += "<F>\n";
			text += generateFormCode(elements);
			text += "</F>\n";
			text += "</C>";
			
//			text = makeHtmlReady(text); //if commented out, also comment out line 45 : setupStyle()
//			text = addCssTags(text);
			// watch out, conflict with communication through editer.updatecode
			
			codeBox.text = text;
			
			if(State.time != 0)
				codeBox.text = "Mettez le temps sur 0 secondes.";

		}
		
		public function generateGroupCode(elements:Array):String
		{
			var text:String = "";
			for each(var group:Element in elements){
				if(group is Group)
					text += group.getCode()+"\n";
			}			
			return text;
		}
		
		public function generateFormCode(elements:Array):String
		{
			var text:String = "";
 
			for each(var shape:Element in elements){
				if(shape is Shape)
					text += "\t"+shape.getCode()+"\n";
			}
			return text;
		}
		
		public function switchSize(e:MouseEvent):void
		{
			if(codeBox.height == smallHeight)
			{
				codeBox.x = bigX;
				codeBox.y = bigY;
				codeBox.height = bigHeight;
				codeBox.width = bigWidth;
			}
			else{
				codeBox.x = smallX;
				codeBox.y = smallY;
				codeBox.height = smallHeight;
				codeBox.width = smallWidth;
			}
			drawBackground();
		}
		
		public function copyCodeToClipboard(e:Event):void
		{
			var clipboard:Clipboard = Clipboard.generalClipboard;
			clipboard.clear();
			clipboard.setData(ClipboardFormats.TEXT_FORMAT, codeBox.text);
		}
		
		private function drawBackground():void
		{
			graphics.clear();
			graphics.lineStyle(1);
			graphics.beginFill(Properties.codeBackgroundColor,0.7);
			graphics.drawRect(codeBox.x, codeBox.y, codeBox.width, codeBox.height);
			graphics.endFill();
		}
		
		private function makeHtmlReady(code:String):String
		{
			var result:String = "";
			
			for(var i:int = 0; i <code.length; i++){
				if(code.charAt(i) == "<")
					result += "&lt;";
				else if(code.charAt(i) == ">")
					result += "&gt;";
				else result += code.charAt(i);
			}
			return result;
		}
		private function addCssTags(code:String):String
		{
			var result:String = "";
			var p:int = 0;
			for(var i:int = 0; i <code.length; i++){
				if(code.charAt(i) == "\"" && p == 0){
					result+= "\"<span class='params'>";
					p = 1;
				}
				else if(code.charAt(i) == "\"" && p == 1){
					result+= "</span>\"";
					p = 0;
				}
				else if(code.charAt(i) == "L")
					result+= "<span class ='L'>L</span>";
				else if(code.charAt(i) == "R")
					result+= "<span class ='R'>R</span>";
				else if(code.charAt(i) == "E")
					result+= "<span class ='E'>E</span>";
				else if(code.charAt(i) == "C")
					result+= "<span class ='C'>C</span>";
				else if(code.charAt(i) == "G")
					result+= "<span class ='G'>G</span>";
				else result += code.charAt(i);
			}
			return "<body>"+result+"</body>";
		}
		
		public function get code():String
		{
			return codeBox.text;
		}
		
		public function set code(value:String):void
		{
			codeBox.text = value;
		}

		public function get parser():CodeParser
		{
			return _codeParser;
		}
	}
}