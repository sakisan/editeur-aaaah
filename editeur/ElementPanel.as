package editeur
{
	import blocks.*;
	
	import flash.display.MovieClip;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import properties.Properties;
	
	import support.InputField;
	import support.MyCheckBox;
	
	public class ElementPanel extends MovieClip
	{
		private var element:Element;
		private var xPos:InputField;
		private var yPos:InputField;
		private var thickness:InputField;
		private var xDir:InputField;
		private var yDir:InputField;
		private var xBend:InputField;
		private var yBend:InputField;
		private var full:MyCheckBox;
		private var myWidth:InputField;
		private var myHeight:InputField;
		
		public function ElementPanel(element:Element, x:int, y:int, width:int, height:int)
		{
			this.element = element;
			
			var title:TextField = new TextField;
			title.defaultTextFormat = Properties.standardFormat;
			title.x = x;
			title.y = y;
			title.width = width;
			title.height = 20;
			addChild(title);
			
			xPos = new InputField(x,y+25,80, 20, "Position x :");
			xPos.value = element.xPosition;
			addChild(xPos);
			xPos.addEventListener(KeyboardEvent.KEY_UP, xPosHandler);
			
			yPos = new InputField(x,y+50,80, 20, "Position y :");
			yPos.value = element.yPosition;
			addChild(yPos);
			yPos.addEventListener(KeyboardEvent.KEY_UP, yPosHandler);
			
			if(element is Group){
				title.text = "GROUP";
			}
			if(element is Shape){
				thickness = new InputField(x,y+75,80, 20, "Épaisseur :");
				thickness.value = Shape(element).thickness;
				addChild(thickness);
				thickness.addEventListener(KeyboardEvent.KEY_UP, thicknessHandler);
			}
			if(element is Line){
				title.text = "LIGNE";
				
				xDir= new InputField(x,y+100,80, 20, "Direction x :");
				xDir.value = Line(element).xDirection;
				addChild(xDir);
				xDir.addEventListener(KeyboardEvent.KEY_UP, xDirHandler);
				
				yDir= new InputField(x,y+125,80, 20, "Direction y :");
				yDir.value = Line(element).yDirection;
				addChild(yDir);
				yDir.addEventListener(KeyboardEvent.KEY_UP, yDirHandler);
			}
			if(element is Curve){
				title.text = "COURBE";
				
				xBend= new InputField(x,y+150,80, 20, "Courbure x :");
				xBend.value = Curve(element).xBend;
				addChild(xBend);
				xBend.addEventListener(KeyboardEvent.KEY_UP, xBendHandler);
				
				yBend= new InputField(x,y+175,80, 20, "Courbure y :");
				yBend.value = Curve(element).yBend;
				addChild(yBend);
				yBend.addEventListener(KeyboardEvent.KEY_UP, yBendHandler);
			}
			if(element is FillShape){
				myWidth= new InputField(x,y+100,80, 20, "Largeur :");
				myWidth.value = FillShape(element).myWidth;
				addChild(myWidth);
				myWidth.addEventListener(KeyboardEvent.KEY_UP, myWidthHandler);
				
				myHeight= new InputField(x,y+125,80, 20, "Hauteur :");
				myHeight.value = FillShape(element).myHeight;
				addChild(myHeight);
				myHeight.addEventListener(KeyboardEvent.KEY_UP, myHeightHandler);
				
				full= new MyCheckBox(x,y+150,"Remplis");
				full.selected = FillShape(element).full == 1;
				addChild(full);
				full.addEventListener(MouseEvent.MOUSE_DOWN, fullHandler);
			}
			if(element is Rectangle)
				title.text = "RECTANGLE";
			if(element is Ellipse)
				title.text = "ELLIPSE";
		}
		
		public function xPosHandler(e:KeyboardEvent):void
		{
			if(element is Group){
				var p:String = xPos.value+","+element.yPosition;
				Group(element).loadParameters(p);
			}else
			element.xPosition = xPos.value;
		}
		
		public function yPosHandler(e:KeyboardEvent):void
		{
			if(element is Group){
				var p:String = element.xPosition+","+yPos.value;
				Group(element).loadParameters(p);
			}else
			element.yPosition = yPos.value;
		}
		public function thicknessHandler(e:KeyboardEvent):void
		{
			Shape(element).thickness = thickness.value;
		}
		public function xDirHandler(e:KeyboardEvent):void
		{
			Line(element).xDirection = xDir.value;
		}
		public function yDirHandler(e:KeyboardEvent):void
		{
			Line(element).yDirection = yDir.value;
		}
		public function xBendHandler(e:KeyboardEvent):void
		{
			Curve(element).xBend = xBend.value;
		}
		public function yBendHandler(e:KeyboardEvent):void
		{
			Curve(element).yBend = yBend.value;
		}
		public function myWidthHandler(e:KeyboardEvent):void
		{
			FillShape(element).myWidth = myWidth.value;
		}
		public function myHeightHandler(e:KeyboardEvent):void
		{
			FillShape(element).myHeight = myHeight.value;
		}
		public function fullHandler(e:MouseEvent):void
		{
			FillShape(element).full = full.selected ? 1: 0;
		}
	}
}