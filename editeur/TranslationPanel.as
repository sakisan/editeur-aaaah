package editeur
{
	import blocks.Animation;
	import blocks.Element;
	import blocks.Translation;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import support.InputField;
	import support.MyButton;
	
	public class TranslationPanel extends AnimationPanel
	{
		private var translationButton:MyButton;
		private var xTarget:InputField;
		private var yTarget:InputField;
		
		private var element:Element;
		private var myTranslation:Translation;
		
		public function TranslationPanel(element:Element,  x:int, y:int, width:int, height:int)
		{
			this.element = element;
			myTranslation = new Translation;
			myTranslation.loop = 0;
						
			super("Translation",x,y,width,height);
			
			xTarget = new InputField(x, y+25, width+5, height, "Position x ciblée :");
			yTarget = new InputField(x, y+50, width+5, height, "Position y ciblée :");
			
			addChild(xTarget);
			addChild(yTarget);

			
			if(element.myTranslation != null){
				activeCheckbox.selected = true;
				xTarget.value = element.myTranslation.xTarget;
				yTarget.value = element.myTranslation.yTarget;
				delay.value = element.myTranslation.delay;
				time.value = element.myTranslation.time;
			}
			else{
				xTarget.value = element.xPosition;
				yTarget.value = element.yPosition;
				delay.value = 0;
				time.value = 5;
			}
			
			myTranslation.xTarget = xTarget.value;
			myTranslation.yTarget = yTarget.value;
			myTranslation.delay = delay.value;
			myTranslation.time = time.value;
			
			xTarget.addEventListener(KeyboardEvent.KEY_UP,xTargetHandler);
			yTarget.addEventListener(KeyboardEvent.KEY_UP,yTargetHandler);
			
		}
		override protected function setAnimation(e:Event):void
		{
			element.myTranslation = myTranslation;
			activeCheckbox.selected =  true;
		}
		
		override protected function unsetAnimation(e:Event):void
		{
			element.myTranslation = null;
			activeCheckbox.selected = false;
		}
		
		private function xTargetHandler(e:KeyboardEvent):void
		{
			myTranslation.xTarget = xTarget.value;
			setAnimation(e);
		}
		private function yTargetHandler(e:KeyboardEvent):void
		{
			myTranslation.yTarget = yTarget.value;
			setAnimation(e);
		}
		
		override protected function get animation():Animation
		{
			return myTranslation;
		}
	}
}