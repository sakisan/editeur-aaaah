package editeur
{
	import blocks.Animation;
	import blocks.Element;
	import blocks.Rotation;
	import blocks.Translation;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import support.InputField;
	import support.MyButton;
	
	public class RotationPanel extends AnimationPanel
	{

		private var degrees:InputField;

		
		private var element:Element;
		private var myRotation:Rotation;
		
		public function RotationPanel(element:Element, x:int, y:int, width:int, height:int)
		{
			this.element = element; 
			myRotation = new Rotation;
			myRotation.loop = 0;
			
			super("Rotation",x,y,width,height);
			
			degrees = new InputField(x, y+25, width+5, height, "Degrés :");
			addChild(degrees);
			
			if(element.myRotation != null){
				activeCheckbox.selected = true;
				degrees.value = element.myRotation.degrees;
				delay.value = element.myRotation.delay;
				time.value = element.myRotation.time;
			}
			else{
				degrees.value = 0;
				delay.value = 0;
				time.value = 5;
			}
			
			myRotation.degrees = degrees.value;
			myRotation.delay = delay.value;
			myRotation.time = time.value;
			myRotation.loop = 0;
			
			degrees.addEventListener(KeyboardEvent.KEY_UP, degreesHandler);

		}
		
		override protected function setAnimation(e:Event):void
		{
			element.myRotation = myRotation;
			activeCheckbox.selected =  true;
		}
		
		override protected function unsetAnimation(e:Event):void
		{
			element.myRotation = null;
		}
		
		private function degreesHandler(e:KeyboardEvent):void
		{
			myRotation.degrees = degrees.value;
			setAnimation(e);
		}

		
		override protected function get animation():Animation
		{
			return myRotation;
		}
	}
}