package editeur
{
	import blocks.Shape;
	
	import drawing.ShapeDrawerFactory;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.ui.Keyboard;
	
	import properties.Getter;
	import properties.Properties;
	import properties.State;
	
	import support.GroupIterator;
	import support.MyButton;
	
	public class EditPanel extends MovieClip
	{
		private var editer:Editeur1;
		
		private var rest:MovieClip;
		private var smallWidth:Number = 65;
		private var bigWidth:Number = 100;
		private var X:Number = 5;
		private var Y:Number = 440;
		private var h:int = 18;
		private var spacingX:int = 10;
		private var spacingY:int = 6;
		
		private var selectionSubMenu:Sprite;
		private var groupSubMenu:Sprite;
		private var eraserSubMenu:Sprite;
		private var dragSubMenu:Sprite;
		
		public function EditPanel()
		{
			this.editer = Getter.editer;
			
			var outils:TextField = new TextField();
			outils.defaultTextFormat = Properties.yellowFormat;
			outils.text = "Outils";
			outils.x = X;
			outils.y = Y-30;
			addChild(outils);
			
			var options:TextField = new TextField();
			options.defaultTextFormat = Properties.yellowFormat;
			options.text = "Options";
			options.x = X+smallWidth+10;
			options.y = Y-30;
			addChild(options);
			
			selectionSubMenu = new Sprite();
			groupSubMenu = new Sprite();
			eraserSubMenu = new Sprite();
			dragSubMenu = new Sprite();
			
			setupEraser();
			setupSelectionButton();
			setupGroupButton();
			setupDragButton();
			setupOptionsButton();
			
			setupZoneSelection();
			setupDeleteSelection();
			
			setupDegroupButton();
			setupGroupAllButton();
			setupDuplicateButton();
			
			setupEmptyButton();
			setupUndoButton();
			setupRedoButton();

			setupMoveButton();
			setupCenterButton();
			setupTurnButton();
			
			addEventListener(Event.ADDED_TO_STAGE, setupStageListeners);
			
			Getter.editPanel = this;
		}
		
		//SETUP
		private function setupSelectionButton():void
		{
			var x:int = X;
			var y:int = Y;
			
			var selectionButton:MyButton = new MyButton(x,y,smallWidth,h,"Selection F5",Properties.selectionButtonColor);
			addChild(selectionButton);
			selectionButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.selectionON);
			selectionButton.addEventListener(MouseEvent.MOUSE_OVER, showSelectionSubMenu);
			function showSelectionSubMenu():void
			{
				addChild(selectionSubMenu);
				hideOtherSubMenus("selection");
			}
		}
		
		private function setupGroupButton():void
		{
			var x:int = X;
			var y:int = Y+h+spacingY;
			
			var groupButton:MyButton = new MyButton(x,y,smallWidth,h,"Groupe F6",Properties.groupButtonColor);
			addChild(groupButton);
			groupButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.groupSelection);
			groupButton.addEventListener(MouseEvent.MOUSE_OVER, showGroupSubMenu);
			function showGroupSubMenu():void
			{
				addChild(groupSubMenu);
				hideOtherSubMenus("group");
			}
		}		
		
		private function setupEraser():void
		{
			
			var x:int = X;
			var y:int = Y+2*(h+spacingY);
			
			var eraseButton:MyButton = new MyButton(x,y,smallWidth,h,"Gomme F7",Properties.gommeButtonColor);
			addChild(eraseButton);
			eraseButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.eraseON);
			eraseButton.addEventListener(MouseEvent.MOUSE_OVER, showEraserSubMenu);
			function showEraserSubMenu():void
			{
				addChild(eraserSubMenu);
				hideOtherSubMenus("eraser");
			}
		}
		
		private function setupDragButton():void
		{
			var x:int = X;
			var y:int = Y+3*(h+spacingY);
			
			var dragButton:MyButton = new MyButton(x,y,smallWidth,h,"Déplacer F8",Properties.deplacementButtonColor);
			addChild(dragButton);
			dragButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.dragON);
			dragButton.addEventListener(MouseEvent.MOUSE_OVER, showDragSubMenu);
			function showDragSubMenu():void
			{
				addChild(dragSubMenu);
				hideOtherSubMenus("drag");
			}
		}
		
		private function setupOptionsButton():void
		{
			var x:int = X;
			var y:int = Y+4*(h+spacingY);
			
			var optionsButton:MyButton = new MyButton(x,y,smallWidth,h,"Options",Properties.optionsButtonColor);
			addChild(optionsButton);
			optionsButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.showOptions);
		}
		
		private function setupZoneSelection():void
		{
			var x:int = X+smallWidth+spacingX;
			var y:int = Y;
			var h:int = 18;
			var zoneSelection:MyButton = new MyButton(x,y,bigWidth,h,"zone select",Properties.zoneSelectionButtonColor);
			selectionSubMenu.addChild(zoneSelection);
			zoneSelection.addEventListener(MouseEvent.MOUSE_DOWN, editer.zoneSelectionON);
		}
		
		private function setupDeleteSelection():void
		{
			var x:int = X+smallWidth+spacingX;
			var y:int = Y+h+spacingY;
			var deleteSelection:MyButton = new MyButton(x,y,bigWidth,h,"suppr. select.ESC",Properties.deleteSelectionButtonColor);
			selectionSubMenu.addChild(deleteSelection);
			deleteSelection.addEventListener(MouseEvent.MOUSE_DOWN, editer.deleteSelection);
		}
		
		private function setupDegroupButton():void
		{
			var x:int = X+smallWidth+spacingX;
			var y:int = Y;
			
			var degroupButton:MyButton = new MyButton(x,y,bigWidth,h,"Dégroupe",Properties.groupButtonColor);
			groupSubMenu.addChild(degroupButton);
			degroupButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.degroupSelection);
		}	
		
		private function setupGroupAllButton():void
		{
			var x:int = X+smallWidth+spacingX;
			var y:int = Y+h+spacingY;
			var groupAllButton:MyButton = new MyButton(x,y,bigWidth,h,"(dé)Grouper tout",Properties.groupAllButtonColor);
			groupSubMenu.addChild(groupAllButton);
			groupAllButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.groupAll);
		}
		
		private function setupDuplicateButton():void
		{
			var x:int = X+smallWidth+spacingX;
			var y:int = Y+2*(h+spacingY);
			
			var duplicateButton:MyButton = new MyButton(x,y,bigWidth,h,"Dupliquer",Properties.duplicateButtonColor);
			groupSubMenu.addChild(duplicateButton);
			duplicateButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.duplicateSelection);
		}
		
		private function setupEmptyButton():void
		{
			var x:int = X+smallWidth+spacingX;
			var y:int = Y;
			
			var emptyButton:MyButton = new MyButton(x,y,bigWidth,h,"Effacer tout",0xBB0000);
			eraserSubMenu.addChild(emptyButton);
			emptyButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.emptyMap);
		}
		
		private function setupUndoButton():void
		{
			var x:int = X+smallWidth+spacingX;
			var y:int = Y+h+spacingY;
			
			var undoButton:MyButton = new MyButton(x,y,bigWidth,h,"Undo",Properties.undoButtonColor);
			eraserSubMenu.addChild(undoButton);
			undoButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.loadPrevious);
		}
		
		private function setupRedoButton():void
		{
			var x:int = X+smallWidth+spacingX;
			var y:int = Y+2*(h+spacingY);
			
			var redoButton:MyButton = new MyButton(x,y,bigWidth,h,"Redo",Properties.redoButtonColor);
			eraserSubMenu.addChild(redoButton);
			redoButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.loadNext);
		}
		
		private function setupMoveButton():void
		{
			var x:int = X+smallWidth+spacingX;
			var y:int = Y;
			
			var moveButton:MyButton = new MyButton(x,y,bigWidth,h,"Bouger B",Properties.moveButtonColor);
			dragSubMenu.addChild(moveButton);
			moveButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.moveON);
		}
		
		private function setupCenterButton():void
		{
			var x:int =  X+smallWidth+spacingX;
			var y:int = Y+h+spacingY;
			
			var moveButton:MyButton = new MyButton(x,y,bigWidth,h,"Centrer C",Properties.moveButtonColor);
			dragSubMenu.addChild(moveButton);
			moveButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.resetSize);
		}
		
		private function setupTurnButton():void
		{
			var x:int =  X+smallWidth+spacingX;
			var y:int = Y+(h+spacingY)*2;
			
			var turnButton:MyButton = new MyButton(x,y,bigWidth,h,"Tourner",Properties.turnButtonColor);
			dragSubMenu.addChild(turnButton);
			turnButton.addEventListener(MouseEvent.MOUSE_DOWN, editer.turnON);
		}
		
		private function hideOtherSubMenus(active:String):void
		{
			if(active != "selection" && contains(selectionSubMenu))
				removeChild(selectionSubMenu);
			if(active != "group" && contains(groupSubMenu))
				removeChild(groupSubMenu);
			if(active != "eraser" && contains(eraserSubMenu))
				removeChild(eraserSubMenu);
			if(active != "drag" && contains(dragSubMenu))
				removeChild(dragSubMenu);
		}
		
		private function setupStageListeners(e:Event):void
		{
			stage.addEventListener(KeyboardEvent.KEY_UP, keyUp);
			function keyUp(e:KeyboardEvent):void
			{
				var it:GroupIterator;var shape:Shape;
				
				if(e.keyCode == Keyboard.BACKSPACE)
				{
					if(Getter.focusElement != null){
						var drawboard:Drawboard = Getter.drawboard;
						if(drawboard.contains(Getter.focusElement))
							drawboard.setChildIndex(Getter.focusElement, 1);
					}
				}
				if(e.keyCode == Keyboard.ESCAPE)
				{
					editer.deleteSelection(e);
					Getter.focusElement = null;
				}
				if(e.keyCode == Keyboard.F5){
					Getter.drawboard.drawer = ShapeDrawerFactory.createDrawer("Null","Selection");
				}
				if(e.keyCode == Keyboard.F6){
					editer.groupSelection(e);
				}
				if(e.keyCode == Keyboard.F7){
					Getter.drawboard.drawer = ShapeDrawerFactory.createDrawer("Gomme");
				}
				if(e.keyCode == Keyboard.F8){
					Getter.drawboard.drawer = ShapeDrawerFactory.createDrawer("Null","Deplacement");
				}
				if(e.keyCode == 66) //b
				{
					editer.moveON();
				}
				if(e.keyCode == 67) //c
				{
					editer.resetSize();
				}
				if(e.keyCode == 89) //y
				{
					if(e.ctrlKey)
						editer.loadNext(e);
				}
				if(e.keyCode == 90) //z
				{
					if(e.ctrlKey)
						editer.loadPrevious(e);
				}
				if(e.keyCode == Keyboard.NUMPAD_ADD)
				{
					
					it = Getter.selection.iterator;
					while(it.hasNext())
						it.next().changeThickness(1);
				}
				
				if(e.keyCode == Keyboard.NUMPAD_SUBTRACT)
				{
					it = Getter.selection.iterator;
					while(it.hasNext())
						it.next().changeThickness(-1);
				}
				
				if(e.keyCode == Keyboard.LEFT)
				{
					it = Getter.selection.iterator;
					while(it.hasNext()){
						shape = it.next();
						shape.setXY(shape.xPosition-Properties.keyMove,shape.yPosition);
					}
				}
				
				if(e.keyCode == Keyboard.RIGHT)
				{
					it = Getter.selection.iterator;
					while(it.hasNext()){
						shape= it.next();
						shape.setXY(shape.xPosition+Properties.keyMove,shape.yPosition);
					}
				}
				
				if(e.keyCode == Keyboard.UP)
				{
					it = Getter.selection.iterator;
					while(it.hasNext()){
						shape = it.next();
						shape.setXY(shape.xPosition,shape.yPosition-Properties.keyMove);
					}
				}
				
				if(e.keyCode == Keyboard.DOWN)
				{
					it = Getter.selection.iterator;
					while(it.hasNext()){
						shape = it.next();
						shape.setXY(shape.xPosition,shape.yPosition+Properties.keyMove);
					}
				}
			}
			
			stage.addEventListener(MouseEvent.MOUSE_WHEEL, mouseWheel);
			function mouseWheel(e:MouseEvent):void
			{
				if(!Getter.codeRenderer.contains(DisplayObject(e.target)))
					editer.mouseWheelZoom(e);
			}
			
		}
		
	}
}