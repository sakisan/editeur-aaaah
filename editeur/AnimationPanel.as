package editeur
{
	import blocks.Animation;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import properties.Getter;
	
	import support.InputField;
	import support.MyButton;
	import support.MyCheckBox;
	
	public class AnimationPanel extends MovieClip
	{
		private var animationButton:MyButton;
		protected var activeCheckbox:MyCheckBox;
		
		private var boucle:MyCheckBox;
		private var boucleAR:MyCheckBox;
		
		protected var delay:InputField;
		protected var time:InputField;
		
		public function AnimationPanel(name:String,x:int, y:int, width:int, height:int)
		{
			activeCheckbox = new MyCheckBox(x, y, name);
			addChild(activeCheckbox);
			activeCheckbox.addEventListener(MouseEvent.MOUSE_DOWN, button);
			
			delay = new InputField(x, y+75, width+5, height, "Délais :");
			time = new InputField(x, y+100, width+5, height, "Temps :");
			addChild(delay);
			addChild(time);
			delay.addEventListener(KeyboardEvent.KEY_UP, delayHandler);
			time.addEventListener(KeyboardEvent.KEY_UP, timeHandler);
			
			boucle = new MyCheckBox(x, y+125,"Boucle");
			boucleAR = new MyCheckBox(x , y+145, "Aller-Retour");
			addChild(boucle);
			addChild(boucleAR);
			boucle.addEventListener(MouseEvent.MOUSE_DOWN, boucleHandler);
			boucleAR.addEventListener(MouseEvent.MOUSE_DOWN, boucleARHandler);
			
			addEventListener(KeyboardEvent.KEY_UP, showCorrect);
			addEventListener(MouseEvent.MOUSE_UP, showCorrect);
		}
		
		private function button(e:Event):void
		{
			if(!activeCheckbox.selected)
				unsetAnimation(e);
			else setAnimation(e);
		}
		
		private function delayHandler(e:KeyboardEvent):void
		{
			animation.delay = delay.value;
			setAnimation(e);
		}
		private function timeHandler(e:KeyboardEvent):void
		{
			animation.time = time.value;
			setAnimation(e);
		}
		
		private function boucleHandler(e:MouseEvent):void
		{
			if(boucle.selected)
				animation.loop = 1;
			else{
				animation.loop = 0;
				boucleAR.selected = false;
			}
			setAnimation(e);
		}
		private function boucleARHandler(e:MouseEvent):void
		{
			if(boucleAR.selected){
				animation.loop = 2;
				boucle.selected = true;
			}
			else animation.loop = 0;
			setAnimation(e);
		}
		private function noBoucleHandler(e:MouseEvent):void
		{
			animation.loop = 0;
			setAnimation(e);
		}
		
		protected function setAnimation(e:Event):void
		{
			//override
		}
		
		protected function unsetAnimation(e:Event):void
		{
			//override
		}
		
		protected function get animation():Animation
		{
			//override
			return new Animation();
		}
		
		private function showCorrect(e:Event):void
		{
			Getter.timeChanger.timeHandler(e);
		}
	}
}