package editeur
{
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.text.TextField;
	import flash.ui.Keyboard;
	
	import properties.Getter;
	import properties.Properties;
	import properties.State;
	
	import support.InputField;

	public class OptionsPanel extends MovieClip
	{
		private var Y:Number = 50;
		private var X:Number = 50;
		private var W:Number = 500;
		private var H:Number = 20;
		
		public function OptionsPanel()
		{
			graphics.beginFill(Properties.backgroundColor,0.85);
			graphics.drawRect(0,-100,900,700);
			graphics.endFill();
			
			gridChoice();
			moveChoice();
			decimalsChoice();
			//backgroundImageChoice();
			//roomMulti();
		}
		
		private function gridChoice():void
		{
			var field:InputField = makeField("Quadrillage horizontal de la grille",spaceXHandler,Properties.gridSpaceX);
			function spaceXHandler(e:Event):void
			{
				Properties.gridSpaceX = field.value;
				Getter.drawboard.drawHitAreaRectangle(State.color);
			}
			
			
			var field2:InputField = makeField("Quadrillage vertical de la grille",spaceYHandler,Properties.gridSpaceY);
			function spaceYHandler(e:Event):void
			{
				Properties.gridSpaceY = field2.value;
				Getter.drawboard.drawHitAreaRectangle(State.color);
			}
		}
		
		private function moveChoice():void
		{
			var field:InputField = makeField("Déplacement de la sélection via les touches flechées (en pixel)",moveChoiceHandler,Properties.keyMove);
			function moveChoiceHandler(e:Event):void
			{
				Properties.keyMove = field.value;
			}
		}
		
		private function decimalsChoice():void
		{
			var field:InputField = makeField("Nombre de décimales dans les valeurs",decimalChoiceHandler,Properties.decimals);
			function decimalChoiceHandler(e:Event):void
			{
				
				Properties.decimals = int(field.value);
			}
		}
		
		private function backgroundImageChoice():void
		{
			var myfileReference:FileReference = new FileReference();
			var field:InputField = new InputField(X,Y,300,H,"L'image de fond (hors ligne)");
			field.allowNonNumericalValues();
			field.valueString = Properties.backGroundImage;
			field.valueWidth = 300;
			addChild(field);
			field.addEventListener(MouseEvent.MOUSE_DOWN, browseFile);
			Y+=30;
			
			function browseFile (event:Event):void {
				// Use browse( ) method of a FileReference object to open a
				// dialog box to browse local disks for a file.
				myfileReference.browse();
				
			}
			
			// Listen to when Save button has been clicked on
			myfileReference.addEventListener(Event.SELECT, fileSelected);
			
			function fileSelected(event:Event):void {
				var file:FileReference = FileReference(event.target);
				Properties.backGroundImage = "images/"+event.target.name;
				field.valueString = Properties.backGroundImage;
				Getter.drawboard.drawHitAreaRectangle(State.color);
				
			}
			
		}
		
/*		private function roomMulti():void
		{
			var field:InputField = new InputField(X,Y,300,H,"Nom du multi");
			field.allowAlphebticalCharactersOnly();
			field.valueString = "salon";
			field.valueWidth = 300;
			addChild(field);
			field.addEventListener(KeyboardEvent.KEY_DOWN, multiHandler);
			Y+=30;
			
			function multiHandler(e:KeyboardEvent):void
			{
				if(e.keyCode == Keyboard.ENTER){
					Getter.communication.joinMulti(field.valueString);
				}
			}
		}
		*/
		private function makeField(message:String, f:Function, defaultValue:Number):InputField
		{
			var field:InputField = new InputField(X,Y,W,H,message);
			field.value = defaultValue;
			addChild(field);
			field.addEventListener(KeyboardEvent.KEY_UP, f);
			Y+=30;
			return field;
		}
	}
}
