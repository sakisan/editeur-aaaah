package editeur
{
	import drawing.ShapeDrawer;
	import drawing.ShapeDrawerFactory;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import properties.Getter;
	import properties.Properties;
	import properties.State;
	
	import support.InputField;
	import support.MyButton;
	import support.MyCheckBox;
	
	public class DrawingPanel extends MovieClip
	{
		private var editer:Editeur1;
		
		private var _myThickness:InputField;
		private var _myShape:ShapeChoice;
		private var stopButton:MyButton;
		private var _counter:TextField;
		
		private var xPosition:int;
		private var yPosition:int;
		
		public function DrawingPanel(x:int, y:int)
		{
			this.editer = Getter.editer;
			this.xPosition = x;
			this.yPosition = y;
			
			setupDrawerChoice();
			setupThickness();
			setupFullAndEmptyButtons();
			setupStopButton();
			setupCounter();
		}
		
		private function setupDrawerChoice():void
		{
			myShape = new ShapeChoice(xPosition, yPosition);
			addChild(myShape);
			drawfield.thickness = 3;
			myShape.addEventListener(MouseEvent.MOUSE_DOWN, chooseShape);
		}
		
		private function setupThickness():void
		{
			myThickness = new InputField(xPosition-5,yPosition + myShape.height + 10, 60, 20, "Épaisseur");
			myThickness.value = 3;
			addChild(myThickness);
			myThickness.addEventListener(KeyboardEvent.KEY_UP, updateThickness);
			myThickness.addEventListener(FocusEvent.FOCUS_OUT, thicknessFocusOut);
		}
		
		private function setupFullAndEmptyButtons():void
		{
			var checkBox:MyCheckBox;
			checkBox = new MyCheckBox(xPosition,yPosition + myShape.height + 35);
			checkBox.label = "Remplis";
			checkBox.selected = true;
			checkBox.addEventListener(MouseEvent.MOUSE_DOWN, listen);
			addChild(checkBox);
			function listen(e:MouseEvent):void
			{
				if(checkBox.selected)
					State.full = true;
				else State.full = false;
			}
		}
		
		private function setupStopButton():void
		{
			var y:int = yPosition + myShape.height + 75;
			var width:int = 80;
			var height:int = 35;
			stopButton = new MyButton(xPosition, y, width, height, "Retour \na l'editeur", 0xCC3322);
			stopButton.addEventListener(MouseEvent.MOUSE_DOWN, stopListener);
			addChild(stopButton);
			stopButton.graphics.lineStyle(0);
			stopButton.graphics.beginFill(0xFFFFFF,0.02);
			stopButton.graphics.drawRect(xPosition -5 , y - 5, width+10, height +10);
			stopButton.graphics.endFill();
		}
		
		private function setupCounter():void
		{
			counter = new TextField;
			counter.defaultTextFormat = Properties.inputFormat;
			counter.x = xPosition + 5;
			counter.y = yPosition - 40 ;
			counter.width = 80;
			counter.height = 20;
			addChild(counter);
		}
		
		public function unselect():void
		{
			myShape.unselect();
		}
		
		// EVENTLISTENERS
		private function chooseShape(event:MouseEvent):void {
			drawfield.drawer = ShapeDrawerFactory.createDrawer(myShape.choice);
		}
		
		private function updateThickness(e:Event):void
		{
			var t:int = myThickness.value;
			if(t < 1){
				drawfield.thickness = 1;
				myThickness.value = 1;
			}
			if(t > 255){
				drawfield.thickness = 255;
				myThickness.value = 255;
			}
			else drawfield.thickness = t;
			
		}
		
		private function thicknessFocusOut(e:Event):void
		{
			if(myThickness.value==0)
			{
				drawfield.thickness = 0;
				myThickness.value = 0;
			}
		}
		
		private function stopListener(e:MouseEvent):void
		{
			editer.stopDrawing();
			Getter.drawboard.drawer = ShapeDrawerFactory.createDrawer("Null","Selection");
			showStopButton = true;
			editer.hideOptions(e);
		}
		
		//GETTERS & SETTERS
		
		public function set showStopButton(value:Boolean):void
		{
			if(value)
				addChild(stopButton);
			else{
				removeChild(stopButton);
			}
		}
		
		private function get myShape():ShapeChoice
		{
			return _myShape;
		}
		
		private function set myShape(value:ShapeChoice):void
		{
			_myShape = value;
		}
		
		public function get myThickness():InputField
		{
			return _myThickness;
		}
		
		public function set myThickness(value:InputField):void
		{
			_myThickness = value;
		}

		public function get drawfield():Drawboard
		{
			return editer.drawfield;
		}

		public function set drawfield(value:Drawboard):void
		{
			editer.drawfield = value;
		}

		public function get counter():TextField
		{
			return _counter;
		}

		public function set counter(value:TextField):void
		{
			_counter = value;
		}


	}
}