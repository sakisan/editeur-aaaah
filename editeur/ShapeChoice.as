package editeur
{
	import drawing.ShapeDrawer;
	import drawing.ShapeDrawerFactory;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Keyboard;

	import properties.Getter;
	import properties.Properties;
	
	import support.MyButton;
	import support.MyCheckBox;
	
	public class ShapeChoice extends MovieClip
	{
		private var _choice:String;
		private var myCheckBoxes:Array = [];
		private var allShapes:Array = ["Ligne","Courbe","Rectangle","Ellipse","Polygone"];
		
		public function ShapeChoice(xPos:int, yPos:int)
		{
			var x:int = xPos;
			var y:int = yPos;
			var width:int = 70;
			
			for each(var msg:String in allShapes){
				var checkBox:MyCheckBox = new MyCheckBox(x,y, msg);
				myCheckBoxes.push(checkBox);
				addChild(checkBox);
				checkBox.addEventListener(MouseEvent.MOUSE_DOWN, setChoice);
				y+=18+6;
			}
			
			MyCheckBox(myCheckBoxes[0]).selected=true;
			addEventListener(Event.ADDED_TO_STAGE, setupListeners);
		}
		
		private function setChoice(e:MouseEvent):void
		{
			var checkBox:MyCheckBox = MyCheckBox(e.target);
			choice = allShapes[myCheckBoxes.indexOf(checkBox)];
			unselect();
			checkBox.selected = true;
		}
		
		public function unselect():void
		{
			for each(var b:MyCheckBox in myCheckBoxes)
			b.selected = false;
		}
		
		public function select(shape:String):void
		{
			unselect();
			MyCheckBox(myCheckBoxes[allShapes.indexOf(shape)]).selected = true;
		}
		
		private function setupListeners(e:Event):void
		{
			stage.addEventListener(KeyboardEvent.KEY_UP, keyUp);
		}
		
		private function keyUp(e:KeyboardEvent):void
		{
			var drawboard:Drawboard = Getter.drawboard;
			if(e.keyCode == Keyboard.F1){
				drawboard.drawer = ShapeDrawerFactory.createDrawer("Ligne");
				select("Ligne");
			}
			else if(e.keyCode == Keyboard.F2){
				drawboard.drawer = ShapeDrawerFactory.createDrawer("Courbe");
				select("Courbe");
			}
			else if(e.keyCode == Keyboard.F3){
				drawboard.drawer = ShapeDrawerFactory.createDrawer("Rectangle");
				select("Rectangle");
			}
			else if(e.keyCode == Keyboard.F4){
				drawboard.drawer = ShapeDrawerFactory.createDrawer("Ellipse");
				select("Ellipse");
			}
			
		}
		
		// GETTERS & SETTERS
		
		public function get choice():String
		{
			return _choice;
		}

		public function set choice(value:String):void
		{
			_choice = value;
		}

	}
}