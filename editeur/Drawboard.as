package editeur
{
	import blocks.*;
	
	import drawing.*;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import flash.ui.Keyboard;
	
	import properties.Getter;
	import properties.Properties;
	import properties.State;
	
	import support.ElementsIterator;
	import support.GroupIterator;
	
	public class Drawboard extends MovieClip
	{
		private var editer:Editeur1;
		
		private var _drawer:ShapeDrawer;
		private var _elements:Array;
		private var _shapeCounter:int;
		private var _thickness:int;
		private var hitAreaRectangle:Sprite;
		
		public function Drawboard()
		{
			this.editer = Getter.editer;
			setupHitArea();
			drawer = new LineDrawer(this);
			addChild(drawer);
			_elements = new Array();
			this.opaqueBackground = Properties.backgroundColor;
			addEventListener(Event.ADDED_TO_STAGE, setupListeners);
		}
		
		
		private function setupHitArea():void
		{
			hitAreaRectangle = new Sprite();
			drawHitAreaRectangle(0x000000);
			addChild(hitAreaRectangle);
		}
		
		public function drawHitAreaRectangle(color:uint):void
		{
			var outline:BitmapData;
			
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoaded);
			loader.load(new URLRequest(Properties.backGroundImage));
			
			function imageLoaded (event:Event):void
			{
				outline = Bitmap(LoaderInfo(event.target).content).bitmapData;
				hitAreaRectangle.graphics.clear();
				hitAreaRectangle.graphics.lineStyle(3,color);
				hitAreaRectangle.graphics.beginFill(Properties.backgroundColor);
				hitAreaRectangle.graphics.drawRect(-160000,-160000,320000,320000);
				hitAreaRectangle.graphics.endFill();
				hitAreaRectangle.graphics.beginBitmapFill(outline, null, false, true);
				hitAreaRectangle.graphics.drawRect(0,0,800,400);
				if(Properties.gridON)
					drawGrid(hitAreaRectangle.graphics, color);
				hitAreaRectangle.graphics.endFill();
			}
		}

		private function drawGrid(grafics:Graphics, color:uint):void
		{	
			var spaceX:int = Properties.gridSpaceX;
			var spaceY:int = Properties.gridSpaceY;
			
			grafics.lineStyle(1,0x101010);
			
			if(spaceX > 0)
			for(var i:int = 0; i< 800; i+=spaceX){
				grafics.moveTo(i,0);
				grafics.lineTo(i,400);
			}
			if(spaceY > 0)
			for(var j:int = 0; j< 400; j+=spaceY){
				grafics.moveTo(0, j);
				grafics.lineTo(800,j);
			}
			
			grafics.moveTo(-20000, 0);
			grafics.lineStyle(1,color);
			grafics.lineTo(20000,0);
			grafics.moveTo(-20000, 400);
			grafics.lineTo(20000,400);
			grafics.moveTo(0,-20000);
			grafics.lineTo(0,20000);
			grafics.moveTo(800,-20000);
			grafics.lineTo(800,20000);
		}
		
		public function addElement(element:Element, update:Boolean = true):void
		{
			var numberOfShapes:int;
			if(element is Group)
				numberOfShapes = Group(element).shapes.length;
			else numberOfShapes = 1;
			
			if(shapeCounter + numberOfShapes <= Properties.maxShapes){
				addChild(element);
				elements.push(element);
			}
			if(update)
				editer.updateCode();
		}
		
		public function eraseShape(shape:Shape, authorization:Boolean = false):void
		{
			if(elements.indexOf(shape) > -1 && ( drawer is EraseDrawer || authorization)){
				removeChild(shape);
				elements.splice(elements.indexOf(shape),1);
				editer.updateCode();
			}
		}
		
		public function eraseGroup(group:Group, authorization:Boolean = false):void
		{
			if(drawer is EraseDrawer || authorization){
				if(group.parent == this)
					removeChild(group);
				if(elements.indexOf(group) > -1)
					elements.splice(elements.indexOf(group),1);
				editer.updateCode();
			}
		}
		
		public function createGroupFromSelection(selection:Selection):Group
		{
			var group:Group = new Group(this, selection);
			if(group.shapes.length > 1){
				elements.unshift(group);
				addChild(group);
			}
			return group;
		}
		
		private function updateCounter():void
		{
			var counter:int = 0;
			for each(var e:Element in elements){
				if(e is Group)
					counter+= Group(e).shapes.length;
				if(e is Polygon)
					counter += Polygon(e).lines.shapes.length-1;
				else counter ++;
			}
			shapeCounter = counter;
		}
		
		public function drawing():Boolean
		{
			return drawer.drawing();
		}
				
		//EVENTLISTENER
		
		private function clickHandler(e:MouseEvent):void
		{
			if(!editer.drawingPanel.contains(DisplayObject(e.target)))
				drawer.clickHandler(e);
			if(drawing())
				editer.hideDrawingPanel();
			if(!drawing())
				editer.showDrawingPanel();
		}
		
		private function moveHandler(e:MouseEvent):void
		{
			drawer.moveHandler(e);
		}
		
		private function setupListeners(e:Event):void
		{
			stage.addEventListener(MouseEvent.MOUSE_DOWN, clickHandler);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, moveHandler);
		}
		
		// GETTERS & SETTERS
		
		public function get drawer():ShapeDrawer
		{
			return _drawer;
		}
		
		public function set drawer(value:ShapeDrawer):void
		{
			if(drawer != null){
				removeChild(drawer);
			}
			_drawer = value;
			addChild(_drawer);
			
			if(Getter.initialized){
				editer.state = value.state;
				if(value.isRealShapeDrawer())
					editer.startDrawing();
				else
					editer.stopDrawing();
			}
		}

		public function get thickness():int
		{
			return _thickness;
		}

		public function set thickness(value:int):void
		{
			_thickness = value;
		}

		public function get elements():Array
		{
			return _elements;
		}

		public function set elements(value:Array):void
		{
			while(_elements.length > 0){
				var e:Element = _elements.pop();
				removeChild(e);
			}
			for each(var c:Element in value){
				addElement(c, false);
			}
			editer.updateCode();
		}

		public function get shapeCounter():int
		{
			updateCounter();
			return _shapeCounter;
		}

		public function set shapeCounter(value:int):void
		{
			_shapeCounter = value;
		}
		
		public function get iterator():ElementsIterator
		{
			return new ElementsIterator(elements);
		}
	}
}