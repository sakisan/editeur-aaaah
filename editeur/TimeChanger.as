package editeur
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import properties.Getter;
	import properties.Properties;
	import properties.State;
	
	import support.InputField;
	import support.MyButton;
	
	public class TimeChanger extends MovieClip
	{
		private var time:InputField;
		private var running:Boolean;
		private var timer:Timer;
		
		public function TimeChanger(x:int, y:int)
		{
			running = false;
			
			time = new InputField(x,y, 50, 20, "Temps :");
			addChild(time);
			time.addEventListener(KeyboardEvent.KEY_UP, timeHandler);
			
			var start:MyButton = new MyButton(x , y+25, 50, 20,"Play");
			addChild(start);
			start.addEventListener(MouseEvent.MOUSE_DOWN, startPlaying);
			
			var pause:MyButton = new MyButton(x , y+50, 50, 20,"Pause");
			addChild(pause);
			pause.addEventListener(MouseEvent.MOUSE_DOWN, pausePlaying);
			
			var stop:MyButton = new MyButton(x , y+75, 50, 20,"Stop");
			addChild(stop);
			stop.addEventListener(MouseEvent.MOUSE_DOWN, stopPlaying);
			
			var button:MyButton = new MyButton(x, y, 85,100, "écouler le temps \n(en test)", 0x69CF58);
			addChild(button);
			button.addEventListener(MouseEvent.MOUSE_DOWN, removeButton);
			function removeButton():void
			{
				removeChild(button);
			}
		}
		
		public function timeHandler(e:Event):void
		{
			if( time.value > 120){
				time.value = 120;
			}
			if( time.value < 0){
				time.value = 0;
			}
			State.time = time.value;
		}
		
		private function startPlaying(e:Event):void
		{
			running = true;
			
			var delay:Number = 1000/Properties.playFPS;
			
			timer = new Timer(delay, (120-State.time)*(1000/delay));
			timer.addEventListener(TimerEvent.TIMER, increaseTime);
			timer.start();
			function increaseTime(e:Event):void
			{
				if(running && State.time < 120){
					State.time+=delay/1000;
					time.value = State.time;
				}
			}
		}
		
		private function pausePlaying(e:Event):void
		{
			running = false;
			if(timer != null)
				timer.reset();
		}
		
		private function stopPlaying(e:Event):void
		{
			pausePlaying(e);
			State.time = 0;
			time.value = 0;
		}
	}
}