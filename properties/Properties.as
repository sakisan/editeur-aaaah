package properties
{
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;

	public class Properties
	{
		public static var server:String = "127.0.0.1";
		public static var port:uint = 51213;

//		public static var defaultBackGroundImage:String = "file:///C:/Users/Sakileon/Documents/Extinction/Aaah/Editeur/images/outline.jpg";
		public static var defaultBackGroundImage:String = "http://sakisan.be/aaaah/images/outline.jpg";
		public static var backGroundImage:String = defaultBackGroundImage;

		public static var maxShapes:Number = 220;

		public static var backgroundColor:uint = 0x303036;
		public static var codeBackgroundColor:uint = 0x999999;
		public static var editeurDefaultX:int = 0;
		public static var editeurDefaultY:int = 100;
		public static var zoomPrecision:int = 30;
		public static var playFPS:int = 10;
		public static var decimals:int = 2;
		public static var gridON:Boolean = true;
		public static var gridSpaceX:Number = 100;
		public static var gridSpaceY:Number = 100;
		public static var keyMove:Number = 1;

		//SHAPE COLORS
		public static var shapeTransparency:Number = 0.9;
		public static var defaultColor:uint = 0x000000;
		public static var defaultColorFocus:uint = 0x666666;
		public static var groupFocus:uint = 0x666666;
		public static var selectionColor:uint = 0x0055DD;
		public static var selectionColorFocus:uint = 0x00AAFF;

		//BUTTON COLORS
		public static var selectionButtonColor:uint = 0x0033FF;
		public static var groupButtonColor:uint = 0x00FF00;
		public static var gommeButtonColor:uint = 0xFF0000;
		public static var deplacementButtonColor:uint = 0xFFBC00;
		public static var selectedButton:uint = 0x6977C1;
		public static var unselectedButton:uint = 0xFFFFFF;
		public static var duplicateButtonColor:uint = 0xEA9DAC;
		public static var groupAllButtonColor:uint = 0x800080;
		public static var zoneSelectionButtonColor:uint = 0x0088FF;
		public static var imageButtonColor:uint = 0x808000;
		public static var plusButtonColor:uint = 0x64C168;
		public static var moinsButtonColor:uint = 0xEC8751;
		public static var deleteSelectionButtonColor:uint = 0x000080;
		public static var moveButtonColor:uint = 0x666666;
		public static var undoButtonColor:uint = 0xc79535;
		public static var redoButtonColor:uint = 0xc73484;
		public static var optionsButtonColor:uint = 0x5f4d55;
		public static var turnButtonColor:uint = 0xd3a769;

		//TEXT COLORS
		public static var font:String = "Verdana";
		public static function get standardFormat():TextFormat
		{
			var standardFormat:TextFormat = new TextFormat();
			standardFormat.color = 0xc5c0d2;
			standardFormat.font = font;
			return standardFormat;
		}

		public static function get inputFormat():TextFormat
		{
			var inputFormat:TextFormat = new TextFormat();
			inputFormat.align = TextFormatAlign.CENTER;
			inputFormat.color = 0x039D9D;
			inputFormat.bold = true;
			inputFormat.font = font;
			return inputFormat;
		}

		public static function get yellowFormat():TextFormat
		{
			var yellowFormat:TextFormat = new TextFormat();
			yellowFormat.color = 0xc5c935;
			yellowFormat.size = 14;
			yellowFormat.font = font;
			return yellowFormat;
		}
	}
}
