package properties
{
	import blocks.Element;
	
	import flash.geom.Matrix;
	
	import support.ElementsIterator;

	public class TimeTraveller
	{
		public function TimeTraveller()
		{
		}
		
		public static function moveElements(elements:Array):void
		{
			var delay:int;
			var time:int;
			var loop:int;
			var xTarget:int;
			var yTarget:int;
			var degrees:int;
			var realtime:Number;
			
			var animate:Matrix;
			var it:ElementsIterator = new ElementsIterator(elements);
			while(it.hasNext())
			{
				var element:Element = it.next();
				animate = new Matrix();
				
				if(element.myRotation != null)
				{
					delay = element.myRotation.delay;
					time = element.myRotation.time;
					loop = element.myRotation.loop;
					degrees = element.myRotation.degrees;
					
					animate.translate(-element.rotationCenterX,-element.rotationCenterY);
					
					
					if(State.time <= delay)
					{
//						animate.rotate(0);
					}
					if(State.time > delay && State.time < delay + time)
					{
						animate.rotate( degreesToRadians((State.time - delay)*degrees / time) );
					}
					if(State.time >= delay + time)
					{
						if(loop == 0)
							animate.rotate( degreesToRadians( degrees ));
						if(loop == 1)
							animate.rotate( degreesToRadians(((State.time-delay)%time)*degrees / time) );
						if(loop == 2){
							realtime = (State.time-delay) % (2*time);
							if(realtime > time)
								realtime = 2* time - realtime;
							animate.rotate( degreesToRadians((realtime)*degrees / time) );
						}
					}
					
					
					animate.translate(element.rotationCenterX,element.rotationCenterY);
					element.transform.matrix = animate;
						
				}
				else{
					animate.translate(-element.rotationCenterX,-element.rotationCenterY);
					animate.rotate(0);
					animate.translate(element.rotationCenterX,element.rotationCenterY);
					element.transform.matrix = animate;
				}
				if(element.myTranslation != null)
				{
					delay = element.myTranslation.delay;
					time = element.myTranslation.time;
					loop = element.myTranslation.loop;
					xTarget = element.myTranslation.xTarget;
					yTarget = element.myTranslation.yTarget;
					
					if(State.time >= delay + time)
					{
						
						if(loop == 1)
							realtime = (State.time-delay) % time;
						if(loop == 2){
							realtime = (State.time-delay) % (2*time);
							if(realtime > time)
								realtime = 2*time - realtime;
						}
						realtime += delay;
						if(loop == 0)
							realtime = delay+time;
					}
					if(State.time < delay + time)
					{
						realtime = State.time;
					}
					if(State.time < delay)
					{
						realtime = delay;
					}
						
					
					animate.translate((xTarget-element.xPosition)*(realtime - delay)/time ,
						(yTarget-element.yPosition)*(realtime - delay)/time );
					
					element.transform.matrix = animate;
				}
			}
		}
		
		public static function degreesToRadians(degrees:Number):Number
		{
			return new Number(Math.PI * degrees / 180);
		}
	}
}