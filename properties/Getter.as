package properties
{
	import blocks.Element;
	import blocks.Selection;
	
	import editeur.CodeRenderer;
	import editeur.Drawboard;
	import editeur.EditPanel;
	import editeur.TimeChanger;
	
	import interactivity.Communication;

	public class Getter
	{
		public static var editer:Editeur1;
		public static var drawboard:Drawboard;
		public static var editPanel:EditPanel;
		public static var codeRenderer:CodeRenderer;
		public static var timeChanger:TimeChanger;
		public static var communication:Communication;
		public static var initialized:Boolean; //means we got out of the constructor of Editeur1.as
		private static var _focusElement:Element; //element that is being rolled over by the mouse

		public static function get selection():Selection
		{
			return editer.mySelection;
		}
		
		public static function get focusElement():Element
		{
			var element:Element;
			if(_focusElement == null)
				element = new Element(null);
			else element = _focusElement;
			return element;
		}

		public static function set focusElement(value:Element):void
		{
			_focusElement = value;
		}
		
		public static function stageToDrawfieldX(stageX:Number):Number
		{
			return (stageX - drawboard.x - editer.x)/drawboard.scaleX;
		}
		
		public static function stageToDrawfieldY(stageY:Number):Number
		{
			return (stageY - drawboard.y - editer.y)/drawboard.scaleY;
		}
		
		public static function drawfieldToStageX(drawX:Number):Number
		{
			return drawboard.scaleX * drawX + drawboard.x + editer.x;
		}
		
		public static function drawfieldToStageY(drawY:Number):Number
		{
			return drawboard.scaleY * drawY + drawboard.y + editer.y;
		}
		
		public static function round(value:Number):Number
		{
			var p:int = Math.pow(10,Properties.decimals);
			return Math.round(p*value)/p;
		}
	}
}