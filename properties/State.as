package properties
{
	import flash.display.MovieClip;
	
	public class State extends MovieClip
	{
		private static var _states:Array = new Array(new Array("Dessin","Gomme","Selection","Deplacement","Perspective","Tournage"),
												new Array(0x000000,Properties.gommeButtonColor,Properties.selectionButtonColor,Properties.deplacementButtonColor, Properties.moveButtonColor, Properties.turnButtonColor));
		private static var _state:String;
		private static var _full:Boolean = true;
		private static var _time:Number = 0;
		
		public function State() {
		}
		
		//GETTERS & SETTERS
		
		public static function get color():uint
		{
			return uint(states[1][states[0].indexOf(state)]);
		}

		public static function get states():Array
		{
			return _states;
		}

		public static function set states(value:Array):void
		{
			_states = value;
		}

		public static function get state():String
		{
			return _state;
		}

		public static function set state(value:String):void
		{
			if(states[0].indexOf(value) > -1){
				_state = value;
			}
		}

		public static function get full():Boolean
		{
			return _full;
		}

		public static function set full(value:Boolean):void
		{
			_full = value;
		}

		public static function get time():Number
		{
			return _time;
		}

		public static function set time(value:Number):void
		{
			_time = value;
			Getter.editer.jumpInTime();
		}

	}
}