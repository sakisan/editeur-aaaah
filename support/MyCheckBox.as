package support
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import properties.Properties;
	
	public class MyCheckBox extends MovieClip
	{
		private var text:TextField;
		private var _selected:Boolean;
		
		public function MyCheckBox(x:int, y:int, name:String = "")
		{
			text = new TextField;
			text.defaultTextFormat = Properties.standardFormat;
			text.text = name;
			text.x = x+20;
			text.y = y;
			updateWidth();
			text.height = 18;
			text.mouseEnabled = false;
			addChild(text);
			
			drawBox();
			addEventListener(MouseEvent.MOUSE_DOWN, toggle);
		}
		
		private function drawBox():void
		{
			graphics.clear();
			graphics.beginFill(0x303036);
			graphics.drawRect(text.x-20, text.y, text.width + 20, text.height);
			graphics.endFill();
			graphics.lineStyle(2);
			graphics.drawRect(text.x-20, text.y, 15,15);
			if(selected){
				graphics.lineStyle(2,0x039D9D);
				graphics.moveTo(text.x-16, text.y+4);
				graphics.lineTo(text.x-9, text.y +11);
				graphics.moveTo(text.x-9, text.y+4);
				graphics.lineTo(text.x-16, text.y +11);
			}
		}
		
		private function updateWidth():void
		{
			text.width = 20+5*label.length;
		}
		
		public function toggle(e:MouseEvent = null):void
		{
			//dont forget this object handles its own clicktoggles !
			selected = !selected;	
		}

		public function get label():String
		{
			return text.text;
		}

		public function set label(value:String):void
		{
			text.text = value;
			updateWidth();
		}

		public function get selected():Boolean
		{
			return _selected;
		}

		public function set selected(value:Boolean):void
		{
			_selected = value;
			drawBox();
		}
	}
}