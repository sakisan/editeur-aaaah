package support
{
	import blocks.Element;
	
	public class ElementsIterator 
	{
		private var list : Array;
		
		private var currentIndex : Number;
		
		public function ElementsIterator( list : Array ) {
			this.list = list.concat();
			this.currentIndex = 0;
		}
		
		public function hasNext( ) : Boolean {
			return this.currentIndex < this.list.length;
		}
		
		public function next( ) : Element
		{
			var next:Element = this.list[this.currentIndex];
			
			this.currentIndex++;
			
			return next;
		}
		
	}
}