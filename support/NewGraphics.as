package support
{   
	import flash.display.BlendMode;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	
	import properties.Properties;
	
	public class NewGraphics extends MovieClip
	{
		private var _color:uint;
		private var _thickness:int;
		
		public function NewGraphics()
		{
			color = Properties.defaultColor;
			this.alpha = Properties.shapeTransparency;
		}
		public function getGraphics():Graphics
		{
			return graphics;
		}
		
		public function clear():void
		{
			graphics.clear();
		}
		
		public function get color():uint
		{
			return _color;
		}
		
		public function set color(value:uint):void
		{
			_color = value;
			graphics.lineStyle(thickness, value);
		}

		public function get thickness():int
		{
			return _thickness;
		}

		public function set thickness(value:int):void
		{
			_thickness = value;
			graphics.lineStyle(thickness, color);
		}

	}
}