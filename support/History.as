package support
{
	import flash.utils.getTimer;

	public class History
	{
		private var history:Array;
		private var position:int;
		
		public function History()
		{
			history = new Array();
			position = 0;
			history.push("<C>\r  <G/>\r  <F/>\r</C>");
		}
		
		public function push(entry:String):void
		{
			var xml:XML = new XML(entry)
			entry = ""+xml;
			if(entry != history[position]){
				position++; 
				if(position != history.length) 
					history.splice(position);
				history.push(entry);
			}
		}
		
		public function previous():String
		{
			var result:String;
			if(position > 0){
				result = history[position-1];
				position--;
			}
			else result = history[0];
			return result;
		}
		
		public function next():String
		{
			var result:String;
			if(position < history.length - 2){
				result = history[position+1];
				position++;
			}
			else result = history[history.length-1];
			return result;
		}

	}
}