package support
{
	import blocks.Shape;

	public class GroupIterator 
	{
		private var list : Array;
		
		private var currentIndex : Number;
		
		public function GroupIterator( list : Array ) {
			this.list = list.concat();
			this.currentIndex = 0;
		}
		
		public function hasNext( ) : Boolean {
			return this.currentIndex < this.list.length;
		}
		
		public function next( ) : Shape
		{
			var next:Shape = this.list[this.currentIndex];
			
			this.currentIndex++;
			
			return next;
		}

	}
}