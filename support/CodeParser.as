package support
{	
	import blocks.*;
	
	import editeur.Drawboard;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;

	public class CodeParser 
	{
		private var drawer:Drawboard;
		private var carte:XML;
		private var code:String;
		private var t:int;
		
		public function CodeParser(drawfield:Drawboard)
		{
			this.drawer = drawfield;
			carte = <C><G/><F/></C>;
		}
	
		public function parseMap(load:String):Array
		{
			carte = new XML(load);
			var allGroups:XMLList = carte.child("G"); 
			var allForms:XMLList =  carte.child("F");
			
			var map:Array = new Array;
			
			for(var g:int = 0; g < allGroups.G.length(); g++){
				var group:Group = Group(parseNode(allGroups.G[g]));
				var parameters:String = ""+group.xPosition+","+group.yPosition;
				for(var s:int = 0; s < allGroups.G[g].children().length(); s++){
					group.add(Shape(parseNode(allGroups.G[g].children()[s])));
				} 
				group.updateXY();
				group.loadParameters(parameters);
				group.redraw();
				map.push(group);
			}
			
			for(var i:int=0; i < allForms.children().length(); i++){
				map.push(parseNode(allForms.children()[i]));
			}
			
			return map;
		}
		
		private function parseNode(node:XML):Element
		{
			var element:Element;
			if(node.name() == "G"){
				element = new Group(drawer);
				Group(element).loadParameters(node.@P);
			}
			else{
				if(node.name() == "L")
					element = new Line(drawer,node.@P);
				if(node.name() == "C")
					element = new Curve(drawer,node.@P);
				if(node.name() == "R")
					element = new Rectangle(drawer,node.@P);
				if(node.name() == "E")
					element = new Ellipse(drawer,node.@P);
				if(node.name() == "P")
					element = new Polygon(drawer,node.@P,node.@Z);

				for(var a:int= 0; a < node.children().length(); a++){
					var animation:XML =	node.children()[a];
					if(animation.name() == "R")
						element.myRotation = new Rotation(animation.@P);
					if(animation.name() == "T")
						element.myTranslation = new Translation(animation.@P);
				}
			}
			
			element.redraw();
			return element;
		}

		public function read():String
		{
			code = ""+carte;
			return code;
		}
	}
}