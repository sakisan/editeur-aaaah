package support
{
	import flash.display.CapsStyle;
	import flash.display.Graphics;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	
	public class RectangleGraphics extends NewGraphics
	{
		public function RectangleGraphics()
		{
			super();
//			graphics.lineStyle(3,0x000000, 1, false, LineScaleMode.NORMAL, CapsStyle.SQUARE,JointStyle.MITER,3); // before
//			graphics.lineStyle(OptionLigne.Ep, 0, 1, true, "normal", null, JointStyle.MITER, 3); = EXTINCTION
			graphics.lineStyle(3,0x000000, 1, true, LineScaleMode.NORMAL, null,JointStyle.MITER,3); // after
		}
		
		override public function set color(value:uint):void
		{
			super.color = value;
			graphics.lineStyle(thickness, value, 1, true, LineScaleMode.NORMAL, null ,JointStyle.MITER,3);
		}
		
		override public function set thickness(value:int):void
		{
			super.thickness = value;
			graphics.lineStyle(thickness, color, 1, true, LineScaleMode.NORMAL, null ,JointStyle.MITER,3);
		}
	}
}