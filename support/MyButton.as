package support
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	public class MyButton extends MovieClip
	{
		private var textfield:TextField;
		private var xPosition:int;
		private var yPosition:int;
		private var myWidth:int;
		private var myHeight:int;
		private var color:uint;
		private var borderThickness:int;
		
		public function MyButton(x:int, y:int, width:int, height:int, msg:String, 
							   color:uint = 0xFFFFFF, borderThickness:int = 3)
		{
			this.xPosition = x;
			this.yPosition = y;
			this.myWidth = width;
			this.myHeight = height;
			this.color = color;
			this.borderThickness = borderThickness;
			
			draw();
			
			buttonMode = true;
			mouseChildren = false;
			
			textfield = new TextField;
			var format:TextFormat = new TextFormat;
			format.align = TextFormatAlign.CENTER;
			textfield.defaultTextFormat = format;
			textfield.x = x;
			textfield.y = y;
			textfield.width = width;
			textfield.height = height;
			textfield.text = msg;
			addChild(textfield);
		}
		
		private function draw():void
		{
			graphics.lineStyle(borderThickness);
			graphics.beginFill(color);
			graphics.drawRect(xPosition,yPosition,myWidth,myHeight);
			graphics.endFill();
		}
		
		public function setColor(value:uint):void
		{
			this.color = value;
			draw();
		}
		
		public function get text():String
		{
			return textfield.text;
		}

		public function set text(value:String):void
		{
			textfield.text = value;
		}

	}
}