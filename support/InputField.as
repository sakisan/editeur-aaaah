package support
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import properties.Properties;
	
	public class InputField extends MovieClip
	{
		private var centerFormat:TextFormat;
		private var field:TextField;
		private var fieldInput:TextField;
		private var numerical:Boolean = true;
		
		public function InputField(x:int, y:int, width:int, height:int, text:String)
		{
			
			
			field = new TextField;
			field.defaultTextFormat = Properties.standardFormat;
			field.x = x;
			field.y = y;
			field.width = width;
			field.height = height;
			field.text = text;
			addChild(field);
			
			fieldInput = new TextField();
			fieldInput.type = TextFieldType.INPUT;
			fieldInput.defaultTextFormat = Properties.inputFormat;
			fieldInput.restrict = ".0-9\\-";
			fieldInput.x = field.x + field.width + 5;
			fieldInput.y = field.y;
			fieldInput.height = field.height;
			fieldInput.width = 38;
			fieldInput.border = false;
			addChild(fieldInput);
		}
		
		public function allowAlphebticalCharactersOnly():void
		{
			fieldInput.restrict= "a-zA-Z";
		}
		
		public function allowNonNumericalValues():void
		{
			fieldInput.restrict= "";
		}
		
		public function get value():Number
		{
			return Number(fieldInput.text);
		}
		
		public function set value(i:Number):void
		{
			fieldInput.text = ""+i;
		}
		
		public function get valueString():String
		{
			return fieldInput.text;
		}
		
		public function set valueString(t:String):void
		{
			fieldInput.text = t;
		}
		
		public function set valueWidth(n:Number):void
		{
			fieldInput.width = n;
		}
	}
}