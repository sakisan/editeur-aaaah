package blocks
{
	import editeur.Drawboard;
	
	import flash.display.Graphics;
	
	import properties.Getter;
	
	import support.NewGraphics;

	public class Curve extends Line
	{
		private var _xBend:Number;
		private var _yBend:Number;
		
		public function Curve(d:Drawboard, parameters:String = null)
		{
			super(d);
			loadParameters(parameters);
		}
		
		private function loadParameters(parameters:String):void
		{
			if(parameters != null){
				var params:Array = Element.parseParameters(parameters);
				this.thickness = Number(params[0]);
				this.xPosition = Number(params[1]);
				this.yPosition = Number(params[2]);
				this.xBend = Number(params[3]);
				this.yBend = Number(params[4]);
				this.xDirection = Number(params[5]);
				this.yDirection = Number(params[6]);
			}
		}
		
		override public function redraw():void
		{
			var g:Graphics = newGraphics.getGraphics();
			g.moveTo(0,0);
			g.curveTo(xBend,yBend ,xDirection, yDirection);
		}
		
		override public function turn(angle:Number, refX:Number, refY:Number):void
		{
			super.turn(angle, refX, refY);
			
			var hulplijn:Line;
			
			hulplijn = new Line(drawboard,"1,"+xPosition+", "+yPosition+", "+xBend+", "+yBend);
			hulplijn.turn(angle, hulplijn.xPosition,hulplijn.yPosition);
			
			xBend = hulplijn.xDirection;
			yBend = hulplijn.yDirection;
			
		}
		
		override public function getCode():String
		{
			var s:String  = "<C P=\""+thickness+","
				+xPosition+","
				+yPosition+","
				+xBend+","
				+yBend+","
				+xDirection+','
				+yDirection+"\">";
			s+= getAnimationCode();
			s+="</C>";
			return s;
		}
		
		override public function clone(cloneAnimation:Boolean = true):Element
		{
			var curve:Curve = new Curve(drawboard);
			curve.thickness = thickness;
			curve.xPosition = xPosition;
			curve.yPosition = yPosition;
			curve.xBend = xBend;
			curve.yBend = yBend;
			curve.xDirection = xDirection;
			curve.yDirection = yDirection;
			if(cloneAnimation){
				if(myTranslation != null)
					curve.myTranslation = myTranslation.clone();
				if(myRotation != null)
					curve.myRotation = myRotation.clone();
			}
			curve.drawboard = drawboard;
			return curve;
		}
		
		override public function equals(shape:Shape):Boolean
		{
			var control:Boolean = shape is Curve;
			
			if(control){
				control = control && xBend == Curve(shape).xBend;
				control = control && yBend == Curve(shape).yBend;
			}
			
			return control && super.equals(shape);
		}
		
		//GETTERS & SETTERS

		public function get xBend():Number
		{
			return _xBend;
		}

		public function set xBend(value:Number):void
		{
			_xBend = Getter.round(value);
			changeColorTo(newGraphics.color);
		}

		public function get yBend():Number
		{
			return _yBend;
		}

		public function set yBend(value:Number):void
		{
			_yBend = Getter.round(value);
			changeColorTo(newGraphics.color);
		}

		override public function get rotationCenterX():Number
		{
			var result:Number;
			if(newGraphics.width == xDirection)
				result = xDirection/2;
			else{
				if(xBend*xDirection > 0)
					if(xDirection > 0)
						result = newGraphics.width/2;
					else
						result = -newGraphics.width/2;
				else
					if(xDirection > 0)
						result = xDirection-newGraphics.width/2;
					else
						result = xDirection+newGraphics.width/2;
			}
			
			return result+xPosition;
		}
		
		override public function get rotationCenterY():Number
		{
			var result:Number;
			if(newGraphics.height == yDirection)
				result = yDirection/2;
			else{
				if(yBend*yDirection > 0)
					if(yDirection > 0)
						result = newGraphics.height/2;
					else
						result = -newGraphics.height/2;
				else
					if(yDirection > 0)
						result = yDirection-newGraphics.height/2;
					else
						result = yDirection+newGraphics.height/2;
			}
				
			return result+yPosition;
		}
	}
}