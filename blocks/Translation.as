package blocks
{
	import properties.Getter;

	public class Translation extends Animation
	{
		private var _xTarget:Number;
		private var _yTarget:Number;
		
		public function Translation(parameters:String = null)
		{
			super();	
			loadParameters(parameters);
		}
		
		private function loadParameters(parameters:String):void
		{
			if(parameters != null){
				var params:Array = Element.parseParameters(parameters);
				this.delay = Number(params[0]);
				this.time = Number(params[1]);
				this.xTarget = Number(params[2]);
				this.yTarget = Number(params[3]);
				this.loop = Number(params[4]);
			}
		}
		
		public function clone():Translation
		{
			var translation:Translation = new Translation();
			translation.xTarget = xTarget;
			translation.yTarget = yTarget;
			translation.delay = delay;
			translation.time = time;
			translation.loop = loop;
			return translation;
		}
		
		override public function equals(a:Animation):Boolean
		{
			var control:Boolean = a is Translation;
			
			if(control){
				control = control && xTarget == Translation(a).xTarget;
				control = control && yTarget == Translation(a).yTarget;
			}
			
			return control && super.equals(a);
		}

		public function get xTarget():Number
		{
			return _xTarget;
		}

		public function set xTarget(value:Number):void
		{
			_xTarget = Getter.round(value);
		}

		public function get yTarget():Number
		{
			return _yTarget;
		}

		public function set yTarget(value:Number):void
		{
			_yTarget = Getter.round(value);
		}

		public function getCode():String
		{
			return "<T P=\""+delay+","
				+time+","
				+xTarget+","
				+yTarget+","
				+loop+"\" />";
		}
	}
}