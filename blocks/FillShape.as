package blocks
{
	import editeur.Drawboard;
	
	import properties.Getter;
	
	public class FillShape extends Shape
	{
		private var _full:int;
		private var _myWidth:Number;
		private var _myHeight:Number;
		
		public function FillShape(d:Drawboard)
		{
			_myHeight = 0;
			_myWidth = 0;
			super(d);
		}
		
		override public function equals(shape:Shape):Boolean
		{
			var control:Boolean = shape is FillShape;
			
			if(control){
				control = control && full == FillShape(shape).full;
				control = control && myWidth == FillShape(shape).myWidth;
				control = control && myHeight == FillShape(shape).myHeight;
			}
			
			return control && super.equals(shape);
		}
		
		override public function turn(angle:Number, refX:Number, refY:Number):void
		{
			var offset:Boolean = Math.abs(refX - rotationCenterX) > 0.005 || Math.abs(refY - rotationCenterY) > 0.005;
			
			var newWidth:Number = 0;
			var newHeight:Number = 0;
			if( (angle < Math.PI/4 && angle > -Math.PI/4)|| ( angle > 3*Math.PI/4 && angle < 5*Math.PI/4 ))
			{
				//don't turn
			}
			else{
				newWidth = myHeight;
				newHeight = myWidth;
				myWidth = newWidth;
				myHeight = newHeight;
			}
			
			if(offset)
			{
				var xD:Number = this.rotationCenterX - refX;
				var yD:Number = this.rotationCenterY - refY;
				var line:Line = new Line(drawboard,"1,"+(refX-xD)+","+(refY-yD)+","+(xD*2)+","+(yD*2));
				line.turn(angle, line.rotationCenterX, line.rotationCenterY);
				this.xPosition = line.xPosition + line.xDirection;
				this.yPosition = line.yPosition + line.yDirection;
			}
			xPosition -= (newWidth - newHeight)/2;
			yPosition -= (newHeight - newWidth)/2;
		}
		
		//GETTERS & SETTERS
		
		override public function get myWidth():Number
		{
			return _myWidth;
		}
		
		public function set myWidth(value:Number):void
		{
			_myWidth = Getter.round(value);
			changeColorTo(newGraphics.color);
		}
		
		override public function get myHeight():Number
		{
			return _myHeight;
		}
		
		public function set myHeight(value:Number):void
		{
			_myHeight = Getter.round(value);
			changeColorTo(newGraphics.color);
		}

		public function get full():int
		{
			return _full;
		}
		
		public function set full(value:int):void
		{
			_full = value;
			changeColorTo(newGraphics.color);
		}
	}
}