package blocks
{
	import editeur.Drawboard;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.Matrix;
	
	import properties.Getter;
	import properties.TimeTraveller;
	
	public class Element extends MovieClip
	{
		private var _xPosition:Number;
		private var _yPosition:Number;
		private var _drawboard:Drawboard;
		private var _myTranslation:Translation;
		private var _myRotation:Rotation;
		private var _ghost:Element;
		
		public function Element(d:Drawboard)
		{
			drawboard = d;
		}
		

		static public function parseParameters(parameters:String):Array
		{//		var InfoGroupe:Array = GroupeXML.attributes.P.split(","); !!!!!
		//		Ok... I had this before i knew about split() so I kept it ^^
			var params:Array = new Array;
			var i:int = 0;
			while(parameters.lastIndexOf(",") > 0)
			{
				params[i] = parameters.substring(0,parameters.indexOf(","));
				parameters = parameters.substring(parameters.indexOf(",")+1);
				i++;
			}
			params[i] = parameters;
			return params;
		}
		
		public function changeColorTo(color:uint):void
		{
			//to be overriden
		}
		
		public function resetColor():void
		{
			//to be overriden
		}
		
		public function getCode():String
		{
			var s:String = "";
			return s;
			//to be overriden
		}
		
		public function getAnimationCode():String
		{
			var s:String ="";
			if(myRotation != null)
				s+=myRotation.getCode();
			if(myTranslation != null)
				s+=myTranslation.getCode();
			return s;
		}
		
		public function redraw():void
		{
			//to be overriden	
		}
		
		public function turn(angle:Number, refX:Number, refY:Number):void
		{
			//to be overriden	
		}
		
		public function updateGhost(newGhost:Boolean = false):void
		{
			if(myTranslation != null || myRotation != null){
				if(ghost == null || newGhost){
					ghost = this.clone(false); 
					ghost.mouseChildren = false;
				}
				if(myTranslation != null)
					ghost.setXY(myTranslation.xTarget, myTranslation.yTarget);
				if(myRotation != null)
				{
					if(myTranslation == null)
						ghost.setXY(xPosition, yPosition);
					var rotate:Matrix = new Matrix();
					rotate.translate(-ghost.rotationCenterX,-ghost.rotationCenterY);
					rotate.rotate( TimeTraveller.degreesToRadians( myRotation.degrees ));
					rotate.translate(ghost.rotationCenterX,ghost.rotationCenterY);
					ghost.transform.matrix = rotate;
				}
			}
			else {
				ghost = null;
			}
		}
		
		public function setXY(X:Number, Y:Number):void
		{
			//to be overriden
		}
		
		public function erase():void
		{
			//to be overriden
		}
		
		public function changeThickness(units:int):void
		{
			//to be overriden
		}
		
		public function clone(cloneAnimation:Boolean = true):Element
		{
			//to be overriden
			return this;
		}
		
		//GETTERS & SETTERS
		
		public function get xPosition():Number
		{
			return _xPosition;
		}
		
		public function set xPosition(value:Number):void
		{
			_xPosition = Getter.round(value);
		}
		
		public function get yPosition():Number
		{
			return _yPosition;
		}
		
		public function set yPosition(value:Number):void
		{
			_yPosition = Getter.round(value);
		}
		
		public function get myHeight():Number
		{
			//override
			return 0;
		}
		
		public function get myWidth():Number
		{
			//override
			return 0;
		}
		
		public function get rotationCenterX():Number
		{
			return xPosition+myWidth/2;
		}
		
		public function get rotationCenterY():Number
		{
			return yPosition+myHeight/2;
		}
		
		public function get drawboard():Drawboard
		{
			return _drawboard;
		}
		
		public function set drawboard(value:Drawboard):void
		{
			_drawboard = value;
		}

		public function get myTranslation():Translation
		{
			return _myTranslation;
		}

		public function set myTranslation(value:Translation):void
		{
			_myTranslation = value;
			updateGhost();
		}

		public function get myRotation():Rotation
		{
			return _myRotation;
		}

		public function set myRotation(value:Rotation):void
		{
			_myRotation = value;
			updateGhost();
		}

		public function get ghost():Element
		{
			return _ghost;
		}

		public function set ghost(value:Element):void
		{
			_ghost = value;
		}

	}
}