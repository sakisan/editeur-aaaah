package blocks
{
	import editeur.Drawboard;
	
	import flash.display.Graphics;
	
	import properties.Getter;

	public class Line extends Shape
	{
		private var _xDirection:Number;
		private var _yDirection:Number;
		
		public function Line(d:Drawboard, parameters:String = null)
		{
			super(d);
			loadParameters(parameters);
		}
		
		private function loadParameters(parameters:String):void
		{
			if(parameters != null){
				var params:Array = Element.parseParameters(parameters);
				this.thickness = Number(params[0]);
				this.xPosition = Number(params[1]);
				this.yPosition = Number(params[2]);
				this.xDirection = Number(params[3]);
				this.yDirection = Number(params[4]);
			}
		}
		
		override public function redraw():void
		{
			var g:Graphics = newGraphics.getGraphics();
			g.moveTo(0,0);
			g.lineTo(xDirection,yDirection);
		}
		
		override public function turn(angle:Number, refX:Number, refY:Number):void
		{
			var offset:Boolean = Math.abs(refX - rotationCenterX) > 0.005 || Math.abs(refY - rotationCenterY) > 0.005;
			if(offset)
				super.turn(angle, refX, refY);
			
			var length:Number = xDirection*xDirection + yDirection*yDirection;
			length = Math.sqrt(length);
			
			var teta:Number;
			
			teta = Math.atan(yDirection/xDirection);
			if(xDirection < 0) teta += Math.PI;
			
			var gamma:Number = teta + angle;
			
			var newXDirection:Number = length*Math.cos(gamma);
			var newYDirection:Number = length*Math.sin(gamma);
			
			if(!offset)
			{
				xPosition = xPosition + xDirection/2 - newXDirection/2;
				yPosition = yPosition + yDirection/2 - newYDirection/2;
			}
			
			xDirection = newXDirection;
			yDirection = newYDirection;
			
			
		}
		
		override public function getCode():String
		{
			var s:String  = "<L P=\""+thickness+","
				+xPosition+","
				+yPosition+","
				+xDirection+','
				+yDirection+"\">";
			s+= getAnimationCode();
			s+="</L>";
			return s;
		}
		
		override public function clone(cloneAnimation:Boolean = true):Element
		{
			var line:Line = new Line(drawboard);
			line.thickness = thickness;
			line.xPosition = xPosition;
			line.yPosition = yPosition;
			line.xDirection = xDirection;
			line.yDirection = yDirection;
			if(cloneAnimation){
				if(myTranslation != null)
					line.myTranslation = myTranslation.clone();
				if(myRotation != null)
					line.myRotation = myRotation.clone();
			}
			line.drawboard = drawboard;
			return line;
		}
		
		override public function equals(shape:Shape):Boolean
		{
			var control:Boolean = shape is Line; 
			
			if(control){
				control = control && xDirection == Line(shape).xDirection;
				control = control && yDirection == Line(shape).yDirection;
			}
			
			return control && super.equals(shape);
		}
		
		//GETTERS & SETTERS

		public function get xDirection():Number
		{
			return _xDirection;
		}

		public function set xDirection(value:Number):void
		{
			_xDirection = Getter.round(value);
			changeColorTo(newGraphics.color);
		}

		public function get yDirection():Number
		{
			return _yDirection;
		}

		public function set yDirection(value:Number):void
		{
			_yDirection = Getter.round(value);
			changeColorTo(newGraphics.color);
		}
		
		override public function get myHeight():Number
		{
			return yDirection;
		}
		
		override public function get myWidth():Number
		{
			return xDirection;
		}
	}
}