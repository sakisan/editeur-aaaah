package blocks
{
	import editeur.Drawboard;
	
	import flash.display.BlendMode;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	import properties.Getter;
	import properties.Properties;
	import properties.State;
	import properties.TimeTraveller;
	
	import support.*;
	
	public class Shape extends Element
	{
		private var _newGraphics:NewGraphics;
		private var _group:Group;
		private var _thickness:Number;
		private var _inSelection:Boolean;
		
		
		public function Shape(d:Drawboard)
		{
			super(d);
			setupGraphics(d);
			group = null;
			addEventListener(MouseEvent.ROLL_OVER, rollOver);
			addEventListener(MouseEvent.ROLL_OUT, rollOut);
		}
		
		protected function setupGraphics(d:Drawboard):void
		{
			newGraphics = new NewGraphics();
			addChild(newGraphics);
			thickness = d.thickness;
			newGraphics.thickness = thickness;
		}
		
		override public function erase():void
		{
			if(group != null)
			{
				group.remove(this);
			}
			else
				drawboard.eraseShape(this);
		}
		
		override public function changeColorTo(color:uint):void
		{
			newGraphics.clear();
			newGraphics.color = color;
			newGraphics.thickness = thickness;
			redraw();
		}
		
		override public function resetColor():void
		{
			if(inSelection)
				changeColorTo(Properties.selectionColor);
			else changeColorTo(Properties.defaultColor);
		}
		
		public function updatePosition():void
		{
			if(group != null){
				xPosition += group.x;
				yPosition += group.y;
				if(group.shapes.indexOf(this) == group.shapes.length-1){
					group.x=0;
					group.y=0;
				}
				group.updateXY(this);
			}
			else{
				xPosition += this.x;
				yPosition += this.y;
				this.x = 0;
				this.y = 0;
			}
		}
		
		public function removeAnimations():void
		{
			myRotation = null;
			myTranslation = null;
		}
		
		override public function redraw():void
		{
			//to be overriden	
		}
		
		override public function turn(angle:Number, refX:Number, refY:Number):void
		{
			if(refX != rotationCenterX || refY != rotationCenterY){
				var xD:Number = this.xPosition - refX;
				var yD:Number = this.yPosition - refY;
				var line:Line = new Line(drawboard,"1,"+(refX-xD)+","+(refY-yD)+","+(xD*2)+","+(yD*2));
				line.turn(angle, line.rotationCenterX, line.rotationCenterY); //important to have rotationCenters here, or we're in for infinite recursion...
				this.xPosition = line.xPosition + line.xDirection;
				this.yPosition = line.yPosition + line.yDirection;
			}
		}
		
		private function rollOver(e:MouseEvent):void
		{
			if(State.state != "Dessin"){
				if(group != null && !hasGroupMemberInSelection){
					group.changeColorTo(Properties.groupFocus);
				}
				if(inSelection)
					changeColorTo(Properties.selectionColorFocus);
				else
					changeColorTo(Properties.defaultColorFocus);
				
				showGhost();
			}
			if(group == null)
				Getter.focusElement = this;
			else Getter.focusElement = this.group;
		}
		
		private function rollOut(e:MouseEvent):void
		{
//			if(this != Getter.focusElement) //not flexible
				resetColor();
			
			if(group != null && group)// != Getter.focusElement)
				group.resetColor();
//			else if(group == Getter.focusElement)
//				changeColorTo(Properties.focusElementColor);
			
			Getter.focusElement = null;
			hideGhost();
		}
		
		override public function clone(cloneAnimation:Boolean = true):Element
		{
			//to be overriden
			return this;
		}
		
		override public function updateGhost(newGhost:Boolean = false):void
		{
			if(group == null){
				super.updateGhost(newGhost);
			}
			else group.updateGhost();
		}
		
		public function showGhost():void
		{
			if(group != null){
				group.showGhost();
			}
			else{
				if(ghost != null){
					ghost.changeColorTo(0xB77EB4);
					drawboard.addChild(ghost);
					drawboard.setChildIndex(this, drawboard.numChildren - 1);
					//					addChild(ghost);
					//					setChildIndex(ghost, 0);
				}
			}
		}
		
		public function hideGhost():void
		{
			if(group == null){
				if(ghost != null)
					if(drawboard.contains(ghost))
						drawboard.removeChild(ghost);
				//					if(contains(ghost))
				//						removeChild(ghost);
			}
			else group.hideGhost();
		}
		
		public function equals(shape:Shape):Boolean
		{
			var control:Boolean =  shape.thickness == this.thickness ;
			
			control = control && xPosition == shape.xPosition;
			control = control && yPosition == shape.yPosition;
			
			if(myTranslation == null ){
				if(shape.myTranslation != null)
					control = false;
			}
			else control = control && myTranslation.equals(shape.myTranslation);
			
			if(myRotation == null ){
				if(shape.myRotation != null)
					control = false;
			}
			else control = control && myRotation.equals(shape.myRotation);
			
			return control;
		}
		
		override public function changeThickness(units:int):void
		{
			thickness += units;
			if(thickness <= 0)
				thickness = 1;
		}
		
		override public function setXY(X:Number, Y:Number):void
		{
			this.xPosition = X;
			this.yPosition = Y;
		}
		
		//GETTERS & SETTERS
		
		override public function set xPosition(value:Number):void
		{
			super.xPosition = value;
			newGraphics.x = value;
			changeColorTo(newGraphics.color);
		}
		
		override public function set yPosition(value:Number):void
		{
			super.yPosition = value;
			newGraphics.y = value;
			changeColorTo(newGraphics.color);
		}
		
		public function get newGraphics():NewGraphics
		{
			return _newGraphics;
		}
		
		public function set newGraphics(value:NewGraphics):void
		{
			_newGraphics = value;
		}
		
		public function get thickness():Number
		{
			return _thickness;
		}
		
		public function set thickness(value:Number):void
		{
			if(value < 1)
				value = 1;
			if(value > 256)
				value = 256;
			_thickness = value;
			newGraphics.thickness = thickness;
			changeColorTo(newGraphics.color);
		}
		
		public function get group():Group
		{
			return _group;
		}
		
		public function set group(value:Group):void
		{
			if(group != null)
				group.remove(this);
			_group = value;
		}
		
		public function get hasGroupMemberInSelection():Boolean
		{
			var hasOne:Boolean = false;
			if(group != null){
				var it:GroupIterator = group.iterator;
				while(it.hasNext() && !hasOne)
					hasOne = hasOne || it.next().inSelection;
			}
			return hasOne;
		}
		
		public function get color():uint
		{
			return newGraphics.color;
		}
		
		public function get inSelection():Boolean
		{
			return _inSelection;
		}
		
		public function set inSelection(value:Boolean):void
		{
			_inSelection = value;
		}
		
	}
}