package blocks
{
	import editeur.Drawboard;
	import flash.display.Graphics;

	public class Ellipse extends FillShape
	{
		
		public function Ellipse(d:Drawboard, parameters:String = null)
		{
			super(d);
			loadParameters(parameters);
		}
		
		private function loadParameters(parameters:String):void
		{
			if(parameters != null){
				var params:Array = Element.parseParameters(parameters);
				this.thickness = int(params[0]);
				this.xPosition = int(params[1]);
				this.yPosition = int(params[2]);
				this.myWidth = int(params[3]);
				this.myHeight = int(params[4]);
				this.full = int(params[5]);
			}
		}
		
		override public function redraw():void
		{
			var g:Graphics = newGraphics.getGraphics();
			if(full)
				g.beginFill(color);
			g.drawEllipse(0,0,myWidth,myHeight);
			g.endFill();
		}
		
		override public function getCode():String
		{
			var s:String  = "<E P=\""+thickness+","
				+xPosition+","
				+yPosition+","
				+myWidth+','
				+myHeight+","
				+ full +"\">";
			s+= getAnimationCode();
			s+="</E>";
			return s;
		}
		
		override public function equals(shape:Shape):Boolean
		{
			var control:Boolean = shape is Ellipse;
			
			return control && super.equals(shape);
		}
		
		override public function clone(cloneAnimation:Boolean = true):Element
		{
			var ellipse:Ellipse = new Ellipse(drawboard);
			ellipse.thickness = thickness;
			ellipse.xPosition = xPosition;
			ellipse.yPosition = yPosition;
			ellipse.myWidth = myWidth;
			ellipse.myHeight = myHeight;
			ellipse.full = full;
			if(cloneAnimation){
				if(myTranslation != null)
					ellipse.myTranslation = myTranslation.clone();
				if(myRotation != null)
					ellipse.myRotation = myRotation.clone();
			}
			ellipse.drawboard = drawboard;
			return ellipse;
		}
	}
}