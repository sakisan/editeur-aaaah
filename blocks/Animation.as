package blocks
{
	public class Animation
	{
		private var _delay:int;
		private var _time:int;
		private var _loop:int;
		
		public function Animation()
		{
		}
		
		public function equals(a:Animation):Boolean
		{
			return a.delay == delay && a.time == time && a.loop == loop;
		}

		public function get delay():int
		{
			return _delay;
		}

		public function set delay(value:int):void
		{
			_delay = value;
		}

		public function get time():int
		{
			return _time;
		}

		public function set time(value:int):void
		{
			_time = value;
		}

		public function get loop():int
		{
			return _loop;
		}

		public function set loop(value:int):void
		{
			_loop = value;
		}


	}
}