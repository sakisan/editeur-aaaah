package blocks
{
	import editeur.Drawboard;
	
	import flash.display.Graphics;
	
	import properties.Getter;
import properties.State;

import support.GroupIterator;

import support.RectangleGraphics;

	public class Polygon extends FillShape
	{
		private var _lines:Group;
		private var xInProgress:Number = 0;
		private var yInProgress:Number = 0;
		private var mayRedraw:Boolean = false;
		public var fictiveLine:Line;

		public function Polygon(d:Drawboard, parameters:String = null, edgeParameters:String = null)
		{
			super(d);
			this.full = State.full ? 1 : 0;
			lines = new Group(d);
			addChild(lines);
			addChild(newGraphics);
			newGraphics.thickness = thickness;
			loadParameters(parameters);
			loadEdgeParameters(edgeParameters);
		}
		
		private function loadParameters(parameters:String):void
		{
			if(parameters != null){
				var params:Array = Element.parseParameters(parameters);
				this.thickness = Number(params[0]);
				this.xPosition = Number(params[1]);
				this.yPosition = Number(params[2]);
				this.myWidth = Number(params[3]);
				this.myHeight = Number(params[4]);
				this.full = Number(params[5]);
				mayRedraw = true;
				removeChild(lines);
			}
		}

		public function loadEdgeParameters(parameters:String):void
		{
			if(parameters != null){
				lines.empty();
				var params:Array = parameters.split(";");
				for(var i:int = 0; i < params.length; i++) {
					var co:Array = String(params[i]).split(",");
					var x:Number = Number(co[0] - xInProgress);
					var y:Number = Number(co[1] - yInProgress);
					addLineXY(x, y);
				}
			}
		}

		public function addLineXY(x:Number, y:Number):void
		{
			var line:Line = new Line(drawboard, "" + thickness + "," + xInProgress + "," + yInProgress + "," + x + "," + y);
			var graphics:Graphics = line.newGraphics.getGraphics();
			graphics.moveTo(0, 0);
			graphics.lineTo(line.xDirection, line.yDirection);
			addLine(line);
		}

		public function addLine(line:Line):void {
			lines.add(line);
			xInProgress += line.xDirection;
			yInProgress += line.yDirection;
			redrawPoly();
		}
		public function roundUp():void
		{
			removeChild(lines);
			fictiveLine = null;
			addLineXY(-xInProgress, -yInProgress);
//			redrawPoly();
		}

		public function redrawPoly():void
		{
			var graphics:Graphics = newGraphics.getGraphics();
			graphics.clear();
			newGraphics.thickness = thickness;
			graphics.moveTo(0, 0);
			if (full) {
				graphics.beginFill(newGraphics.color);
			}
			var xP:Number = 0;
			var yP:Number = 0;

			var it:GroupIterator = lines.iterator;
			while (it.hasNext()) {
				var line:Line = Line(it.next());
				graphics.lineTo(xP + line.xDirection, yP + line.yDirection);
				xP = xP + line.xDirection;
				yP = yP + line.yDirection;
			}

			if(fictiveLine != null && (fictiveLine.xDirection != 0 || fictiveLine.yDirection != 0)){
				graphics.lineTo(xP + fictiveLine.xDirection, yP + fictiveLine.yDirection);
			}

			graphics.lineTo(0, 0);
			graphics.endFill();

			mayRedraw = true;
		}

		override public function redraw():void
		{
			if(mayRedraw) {
				redrawPoly();
			}
		}

		override public function getCode():String
		{
			var s:String = "<P "
							+ getZ()
					+ " P=\"" + thickness + ","
					+ xPosition + ","
					+ yPosition + ","
					+ myWidth + ','
					+ myHeight + ","
					+ full + "\">";
			s += getAnimationCode();
			s += "</P>";
			return s;
		}

		private function getZ():String {
			var s:String = "";
			var it:GroupIterator = lines.iterator;
			var xP:Number = 0;
			var yP:Number = 0;
			var lastLine:String = "";
			while (it.hasNext()) {
				var line:Line = Line(it.next());
				if(s.length > 0){
					s += ";";
				}
				lastLine = (xP + line.xDirection) + "," + (yP + line.yDirection);
                s += lastLine;
				xP = xP + line.xDirection;
				yP = yP + line.yDirection;
			}
//			if(lastLine!="0,0")
//				s+= ";0,0";
			return "Z =\""+s+"\"";
		}
		
		override public function equals(shape:Shape):Boolean
		{
			var control:Boolean = shape is Rectangle;
			
			return control && super.equals(shape);
		}
		
		override public function clone(cloneAnimation:Boolean = true):Element
		{
			var polygon:Polygon = new Polygon(drawboard);
			polygon.thickness = thickness;
			polygon.xPosition = xPosition;
			polygon.yPosition = yPosition;
			polygon.myWidth = myWidth;
			polygon.myHeight = myHeight;
			polygon.full = full;

			if(cloneAnimation){
				if(myTranslation != null)
					polygon.myTranslation = myTranslation.clone();
				if(myRotation != null)
					polygon.myRotation = myRotation.clone();
			}
			polygon.drawboard = drawboard;

			polygon.lines = Group(this.lines.clone());
			return polygon;
		}

		public function get lines():Group {
			return _lines;
		}

		public function set lines(value:Group):void {
			_lines = value;
		}

	}
}