package blocks
{
	import properties.Getter;

	public class Rotation extends Animation
	{
		private var _degrees:Number;
		
		public function Rotation(parameters:String = null)
		{
			super();	
			loadParameters(parameters);
		}
		
		private function loadParameters(parameters:String):void
		{
			if(parameters != null){
				var params:Array = Element.parseParameters(parameters);
				this.delay = Number(params[0]);
				this.time = Number(params[1]);
				this.degrees = Number(params[2]);
				this.loop = Number(params[3]);
			}
		}
		
		public function clone():Rotation
		{
			var rotation:Rotation = new Rotation();
			rotation.degrees = degrees;
			rotation.delay = delay;
			rotation.time = time;
			rotation.loop = loop;
			return rotation;
		}
		
		override public function equals(a:Animation):Boolean
		{
			var control:Boolean = a is Rotation;
			
			if(control){
				control = control && degrees == Rotation(a).degrees;
			}
			
			return control && super.equals(a);
		}

		public function get degrees():Number
		{
			return _degrees;
		}

		public function set degrees(value:Number):void
		{
			_degrees = Getter.round(value);
		}

		public function getCode():String
		{
			return "<R P=\""+delay+","
				+time+","
				+degrees+","
				+loop+"\" />";
		}
	}
}