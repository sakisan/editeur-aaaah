package blocks
{
	import editeur.Drawboard;
	
	import flash.events.Event;
	import flash.geom.Matrix;
	
	import properties.TimeTraveller;
	
	import support.GroupIterator;
	
	public class Group extends Element
	{
		private var _shapes:Array;
		private var _iterator:GroupIterator;
		
		private var highX:Number;
		private var highY:Number;
		private var lowX:Number;
		private var lowY:Number;
				
		public function Group(d:Drawboard, group:Group = null)
		{			
			super(d);
			_shapes = new Array;
			xPosition = 500000;
			yPosition = 500000;
			highX = -500000;
			lowX = 500000;
			highY = -500000;
			lowY = 500000;
			if(group != null)
				addGroup(group);
		}
		
		public function loadParameters(parameters:String):void
		{
			if(parameters != null){
				var params:Array = Element.parseParameters(parameters);
				var xPos:Number = Number(params[0]);
				var yPos:Number = Number(params[1]);
				
				var it:GroupIterator = iterator;
				while(it.hasNext())
				{
					var shape:Shape = it.next();
					shape.xPosition +=  xPos - this.xPosition;
					shape.yPosition +=  yPos - this.yPosition;
				}
				this.xPosition = xPos;
				this.yPosition = yPos;
			}
		}
		
		public function add(shape:Shape):Boolean
		{
			var add:Boolean = shape != null && shapes.indexOf(shape) == -1;
			if(add){
				if(shapes.length == 0){
					if(shape.myTranslation != null)
						this.myTranslation = shape.myTranslation.clone();
					if(shape.myRotation != null)
						this.myRotation = shape.myRotation.clone();
				}
				shape.removeAnimations();
				shapes.push(shape);
				updateXY(shape);
				updateGhost(true);
				if(shape.group == null &&  drawboard != null)
					drawboard.eraseShape(shape, true);
				shape.group = this;
				addChild(shape);
			}
			return add;
		}
		
		public function addGroup(group:Group):Boolean
		{
			var succes:Boolean = false;
			if(group != null && group != this){
				var it:GroupIterator = group.iterator;
				while(it.hasNext()){
					var shape:Shape = it.next();
					if(shape.group != null){
						if(shape.group.myRotation != null)
							myRotation = shape.group.myRotation.clone();
						if(shape.group.myTranslation != null)
							myTranslation = shape.group.myTranslation.clone();
					}
					succes = add(shape);
				}
			}
			validate();
			succes = succes && shapes.length > 1;
			return succes;
		}
		
		public function remove(shape:Shape):void
		{
			var i:int = shapes.indexOf(shape);
			if(i > -1){
				shapes.splice(i, 1);
				shape.group = null;
				if(shape.parent == this)
					removeChild(shape);
				updateXY();
			}
			validate();
		}
		
		public function validate():void
		{
			if(drawboard != null){
				if(shapes.length == 1){
					drawboard.addElement(shapes[0]);
					remove(shapes[0]);
				}
				if(shapes.length == 0)
					drawboard.eraseGroup(this, true);
			}
		}
		
		public function empty():void
		{
			var it:GroupIterator = iterator;
			while(it.hasNext()){
				remove(it.next());
			}
		}

		override public function erase():void
		{
			if(drawboard != null)
				drawboard.eraseGroup(this);
			var it:GroupIterator = iterator;
			while(it.hasNext())
				removeChild(it.next());
		}
		
		public function updateXY(shape:Shape = null):void
		{
			if(shape != null){
				if(shape.xPosition < xPosition) 
					xPosition = shape.xPosition;
				if(shape.yPosition < yPosition)
					yPosition = shape.yPosition;
				if(shape.rotationCenterX+shape.newGraphics.width/2 > highX)
					highX = shape.rotationCenterX+shape.newGraphics.width/2;
				if(shape.rotationCenterY + shape.newGraphics.height/2 > highY)
					highY = shape.rotationCenterY + shape.newGraphics.height/2;
				if(shape.rotationCenterX - shape.newGraphics.width/2 < lowX)
					lowX = shape.xPosition+shape.myWidth;
				if(shape.rotationCenterY - shape.newGraphics.height/2 < lowY)
					lowY = shape.rotationCenterY - shape.newGraphics.height/2;
			}
			else{
				xPosition = 500000;
				yPosition = 500000;
				highX = -500000;
				lowX = 500000;
				highY = -500000;
				lowY = 500000;
				var it:GroupIterator = iterator;
				while(it.hasNext()){
					updateXY(it.next());
				}
			}
		}
		
		public function containsShape(shape:Shape):Boolean
		{
			return shapes.indexOf(shape) != -1;
		}
		
		override public function changeColorTo(color:uint):void
		{
			var it:GroupIterator = iterator;
			while(it.hasNext())
				it.next().changeColorTo(color);
		}
		
		override public function resetColor():void
		{
			var it:GroupIterator = iterator;
			while(it.hasNext())
				it.next().resetColor();
		}
		
		override public function getCode():String
		{
			var s1:String = "";
			var s2:String = "";
			var it:GroupIterator = iterator;
			var s3:String = "";
			if(shapes.length > 0){
				s2+="\t\t"+it.next().getCode();
			    s3 = s2.substring(0,s2.length-4);
				s3+=getAnimationCode();
				s3+=s2.substring(s2.length-4)+"\n";
			}
			while(it.hasNext()){
				s3+="\t\t"+it.next().getCode()+"\n";
			}
			s1 += "\t<G P=\""+xPosition+","+yPosition+"\">\n";
			s1 += s3 + "\t</G>";
			return s1;
		}
		
		override public function redraw():void
		{
			var it:GroupIterator = this.iterator;
			while(it.hasNext())
			{
				var shape:Shape = it.next();
				shape.changeColorTo(shape.newGraphics.color); //also clears before redrawing
			}	
		}
		
		override public function clone(cloneAnimation:Boolean = true):Element
		{
			var group:Group =  new Group(drawboard);
			var it:GroupIterator = this.iterator;
			while(it.hasNext()){
				group.add(Shape(it.next().clone()));
			}
			if(cloneAnimation){
				if(myTranslation != null)
					group.myTranslation = myTranslation.clone();
				if(myRotation != null)
					group.myRotation = myRotation.clone();
			}
			return group;
		}
		
		public function showGhost():void
		{
			if(ghost != null){
				ghost.changeColorTo(0xB77EB4);
				drawboard.addChild(ghost);
				drawboard.setChildIndex(this, drawboard.numChildren - 1);
			}
		}
		
		public function hideGhost():void
		{
			if(ghost != null)
				if(drawboard.contains(ghost))
					drawboard.removeChild(ghost);
		}
		
		override public function changeThickness(units:int):void
		{
			var it:GroupIterator = iterator;
			while(it.hasNext())
			{
				it.next().changeThickness(units);
			}
		}
		
		override public function setXY(X:Number, Y:Number):void
		{
			var parameters:String;
			parameters = ""+X+","+Y;
			this.loadParameters(parameters);
		}
		
		//GETTERS & SETTERS

		public function get shapes():Array
		{
			return _shapes;
		}

		public function get iterator():GroupIterator
		{
			return new GroupIterator(shapes);
		}
		
		override public function get myHeight():Number
		{
			return highX - lowX;
		}
		
		override public function get myWidth():Number
		{
			return highY - lowY;
		}
		
		override public function get rotationCenterX():Number
		{
			return (highX+lowX)/2;
		}
		
		override public function get rotationCenterY():Number
		{
			return (highY+lowY)/2;
		}
	}
}