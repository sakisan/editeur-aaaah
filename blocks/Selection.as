package blocks
{
	import editeur.Drawboard;
	
	import properties.Properties;
	
	import support.GroupIterator;

	public class Selection extends Group
	{
		public function Selection(d:Drawboard)
		{
			super(d);
		}
		
		override public function add(shape:Shape):Boolean 
		{
			var add:Boolean = shape != null && shapes.indexOf(shape) == -1;
			if(add){
				shapes.push(shape);
				shape.changeColorTo(Properties.selectionColor);
				shape.inSelection = true;
			}
			return add;
		}
		
		override public function addGroup(group:Group):Boolean
		{
			var succes:Boolean = false;
			if(group != null && group != this){
				var it:GroupIterator = group.iterator;
				while(it.hasNext()){
					var shape:Shape = it.next();
					succes = add(shape);
				}
			}
//			succes = succes && shapes.length > 1;
			return succes;
		}
		
		override public function remove(shape:Shape):void
		{
			var i:int = shapes.indexOf(shape);
			if(i > -1){
				shapes.splice(i, 1);
			}
			if(shape != null){
				shape.changeColorTo(Properties.defaultColor);
				shape.inSelection = false;
			}
		}
		
		public function containsOneElement():Boolean
		{
			var answer:Boolean = false;
			if(shapes.length != 0)
				if(shapes.length == 1)
					answer = true;
				else {
					answer = true;
					var it:GroupIterator = iterator;
					var group:Group = it.next().group;
					while(it.hasNext() && answer){
						var group2:Group = it.next().group;
						answer = answer && (group == group2);
						group = group2;
					}
					if(group == null)
						answer = false;
					if(answer){
						//selection has only forms from one group
						//but does it have all forms of that group?
						answer = shapes.length == shapes[0].group.shapes.length;
					}
				}
			return answer;
		}
		
		public function updatePositions():void
		{
			var it:GroupIterator = iterator;
			while(it.hasNext()){
				var shape:Shape = it.next();
				shape.updatePosition();
				if(shape.myRotation != null && shape.myTranslation == null)
					shape.updateGhost();
				if(shape.group != null)
					shape.group.updateXY();
			}
		}
	}
}