package blocks
{
	import editeur.Drawboard;
	
	import flash.display.Graphics;
	
	import properties.Getter;
	
	import support.RectangleGraphics;

	public class Rectangle extends FillShape
	{
		
		public function Rectangle(d:Drawboard, parameters:String = null)
		{
			super(d);
			newGraphics = new RectangleGraphics;
			addChild(newGraphics);
			newGraphics.thickness = thickness;
			loadParameters(parameters);
		}
		
		private function loadParameters(parameters:String):void
		{
			if(parameters != null){
				var params:Array = Element.parseParameters(parameters);
				this.thickness = Number(params[0]);
				this.xPosition = Number(params[1]);
				this.yPosition = Number(params[2]);
				this.myWidth = Number(params[3]);
				this.myHeight = Number(params[4]);
				this.full = Number(params[5]);
			}
		}
		
		override protected function setupGraphics(d:Drawboard):void
		{
			d = Getter.drawboard;
			newGraphics = new RectangleGraphics();
			addChild(newGraphics);
			thickness = d.thickness;
			newGraphics.thickness = thickness;
		}
		
		override public function redraw():void
		{
			var g:Graphics = newGraphics.getGraphics();
			if(full)
				g.beginFill(color);
			g.drawRect(0,0,myWidth,myHeight);
			g.endFill();
		}
		
		override public function getCode():String
		{
			var s:String  = "<R P=\""+thickness+","
				+xPosition+","
				+yPosition+","
				+myWidth+','
				+myHeight+","
				+ full +"\">";
			s+= getAnimationCode();
			s+="</R>";
			return s;
		}
		
		override public function equals(shape:Shape):Boolean
		{
			var control:Boolean = shape is Rectangle;
			
			return control && super.equals(shape);
		}
		
		override public function clone(cloneAnimation:Boolean = true):Element
		{
			var rectangle:Rectangle = new Rectangle(drawboard);
			rectangle.thickness = thickness;
			rectangle.xPosition = xPosition;
			rectangle.yPosition = yPosition;
			rectangle.myWidth = myWidth;
			rectangle.myHeight = myHeight;
			rectangle.full = full;
			if(cloneAnimation){
				if(myTranslation != null)
					rectangle.myTranslation = myTranslation.clone();
				if(myRotation != null)
					rectangle.myRotation = myRotation.clone();
			}
			rectangle.drawboard = drawboard;
			return rectangle;
		}

	}
}