package
{
	import blocks.*;
	
	import drawing.*;
	
	import editeur.*;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.engine.SpaceJustifier;
	import flash.ui.Keyboard;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	import flash.utils.setTimeout;
	
	import interactivity.Communication;
	
	import org.osmf.events.TimeEvent;
	
	import properties.*;
	
	import support.*;

	[SWF(width="900", height="700", frameRate="29", backgroundColor="#303036")]
	
	public class Editeur1 extends Sprite
	{	
		private var _drawfield:Drawboard;
		private var _mySelection:Selection;
		private var keepSelection:Selection;
		private var factory:ShapeDrawerFactory;
		
		private var _drawingPanel:DrawingPanel;
		private var editPanel:EditPanel;
		private var selectionPanel:SelectionPanel;
		private var timeChanger:TimeChanger;
		private var options:OptionsPanel;
		
		private var codeRenderer:CodeRenderer;
		private var selectionCode:TextField;
		private var masker:MovieClip;
		private var history:History;
		
		public function Editeur1()
		{					
			this.x = Properties.editeurDefaultX; 
			this.y = Properties.editeurDefaultY; 
			Getter.editer = this;
			Getter.initialized = false;
			drawfield = new Drawboard();
			addChild(drawfield);
			Getter.drawboard = drawfield;
			drawingPanel = new DrawingPanel(810, 10);
			addChild(drawingPanel);
			State.state = "Dessin";
			_mySelection = new Selection(drawfield);
			keepSelection = new Selection(drawfield);
			history = new History();
			
			editPanel = new EditPanel();
			addChild(editPanel);
			timeChanger = new TimeChanger(810, 240);
			addChild(timeChanger);
			Getter.timeChanger = timeChanger;

			setupCode();
			
			options = new OptionsPanel();
			
			addEventListener(MouseEvent.MOUSE_DOWN, clickHandler);
			addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			addEventListener(MouseEvent.MOUSE_MOVE, updateMouseCoordinates);
			doubleClickEnabled = true;
			addEventListener(MouseEvent.DOUBLE_CLICK, selectionON);
			var timer:Timer = new Timer(500);
			timer.start();
			timer.addEventListener(TimerEvent.TIMER ,updateHistory);
			
			setChildIndex(drawingPanel, numChildren-1);
			startDrawing();
			showDrawingPanel();
			
			
			
//			var com:Communication = new Communication();
//			addChild(com); //testing purposes --> migrate that textbox to Getter.as
//			Getter.communication = com;
			
			Getter.initialized = true;
		}
		
		//SETUP
		
		private function setupCode():void
		{			
			codeRenderer = new CodeRenderer(drawfield);
			addChild(codeRenderer);
			codeRenderer.addEventListener(KeyboardEvent.KEY_UP, keyUpLoadCode);
			Getter.codeRenderer = codeRenderer;
			
			selectionCode = new TextField;
			selectionCode.width = 180;
			selectionCode.height = 40;
			selectionCode.x = 00;
			selectionCode.y = 50;
			selectionCode.wordWrap = true;
			selectionCode.mouseEnabled =  false;
			
			addEventListener(Event.ADDED_TO_STAGE, addBox);
			function addBox(e:Event):void
			{
				stage.addChild(selectionCode);
			}
		}
		
		public function startDrawing():void
		{
			codeRenderer.mouseChildren = false;
			editPanel.mouseChildren = false;
			selectionCode.mouseEnabled = false;
			drawfield.drawHitAreaRectangle(0x000000);
			setChildIndex(drawfield, numChildren - 1);
		}
		
		public function stopDrawing():void
		{
			codeRenderer.mouseChildren = true;
			editPanel.mouseChildren = true;
			selectionCode.mouseEnabled = true;
//			selectionON();
			setChildIndex(drawfield, 0);
		}
		
		public function hideDrawingPanel():void
		{
			setChildIndex(drawingPanel, 0);
		}
		
		public function showDrawingPanel():void
		{
			setChildIndex(drawingPanel, numChildren - 1);
		}
		
		public function resetSize(e:Event = null):void
		{
			drawfield.scaleX = 1;
			drawfield.scaleY = 1;
			drawfield.x = 0;
			drawfield.y = 0;
		}
		
		private function reselectForms(step:int):void
		{
			
			if(step == 1){
				keepSelection.empty();
				keepSelection.addGroup(mySelection);
			}
			//code below is to keep non changing forms of the selection in the selection
			else if(step == 2){
				mySelection.empty();
				var sit:GroupIterator = keepSelection.iterator;
				while(sit.hasNext())
				{
					var it:ElementsIterator = drawfield.iterator;
					var shape:Shape = sit.next();
					while(it.hasNext())
					{
						var element:Element = it.next();
						if(element is Shape){
							var newShape:Shape = Shape(element);
							if(shape != null && shape.equals(newShape)){
								mySelection.add(newShape);
								shape = null;
							}
						}
						else if(element is Group){
							var git:GroupIterator = Group(element).iterator;
							while(git.hasNext())
							{
								var newShape2:Shape = git.next();
								if(shape != null && shape.equals(newShape2)){
									mySelection.add(newShape2);
									shape = null;
								}
							}
						}
					}
				}
			}
			
			showSelectionPanelOrNot();
		}
		
		public function jumpInTime():void
		{
			TimeTraveller.moveElements(drawfield.elements);
		}
		
		public function addToSelection(shape:Shape):void
		{
			mySelection.add(shape);
		}
		
		//EventListeners
		
		private function clickHandler(event:MouseEvent):void
		{
			if(State.state == "Dessin" ){
				if(drawfield.drawing()){
					startDrawing();
				}
			}
			if(State.state == "Perspective")
			{
				drawfield.startDrag();
			}
			if(event.target is NewGraphics){
				var shape:Shape = Shape(event.target.parent);
				if(State.state == "Selection"){
					if(mySelection.containsShape(shape))
						mySelection.remove(shape);
					else
						if(shape.group != null && !shape.hasGroupMemberInSelection)
							mySelection.addGroup( shape.group )
						else
							mySelection.add( shape );
				}
				if(State.state == "Gomme"){
					if(shape.group != null){
						Group(shape.group).erase(); 
					}
					else{
						shape.erase();
					}
				}
				if(State.state == "Deplacement"){
					if(shape.group != null){
						shape.group.startDrag();
						mySelection.addGroup(shape.group);
					}
					else{
						shape.startDrag();
						mySelection.add(shape);
					}
				}
			}
		}
		
		private function mouseUpHandler(e:MouseEvent):void
		{
			if(State.state == "Deplacement"){
				mySelection.updatePositions();
				mySelection.stopDrag();
				mySelection.empty();
			}
			if(State.state == "Perspective")
			{
				drawfield.stopDrag();
			}
			//update methods
			if(!codeRenderer.contains(DisplayObject(e.target)))
				updateCode(e);
			showSelectionPanelOrNot();
			updateCounter();
		}
		
		private function updateMouseCoordinates(e:MouseEvent):void
		{			
			selectionCode.text ="("+Getter.round(Getter.stageToDrawfieldX(e.stageX))+","+Getter.round(Getter.stageToDrawfieldY(e.stageY))+")";
		}
		
		public function eraseON(e:Event):void
		{
			drawfield.drawer = ShapeDrawerFactory.createDrawer("Gomme");
		}
		
		public function selectionON(e:Event = null):void
		{
			drawfield.drawer = ShapeDrawerFactory.createDrawer("Null","Selection");
		}
		
		public function moveON(e:Event = null):void
		{
			drawfield.drawer = ShapeDrawerFactory.createDrawer("Null","Perspective");
		}
		
		public function zoneSelectionON(e:Event):void
		{
			drawfield.drawer = ShapeDrawerFactory.createDrawer("SelectionRectangle");
		}
		
		public function groupSelection(e:Event):void
		{
			selectionON(e);
			if(mySelection.shapes.length != 0){
				drawfield.createGroupFromSelection(mySelection);
			}
			mySelection.empty();
		}
		
		public function degroupSelection(e:Event):void
		{
//			var group:Group = new Group(null,mySelection);
//			degroupGroup(group);
			
			if(mySelection.shapes.length > 1){
				var referenceTable:Array = new Array();
				var groupTable:Array = new Array();
				var ungroupedForms:Array = new Array();
				if(mySelection.containsOneElement() 
					&& Shape(mySelection.shapes[0]).group != null){ //overkill? just leave it 
					degroupGroup(Shape(mySelection.shapes[0]).group);
				}
				else{
					var it:GroupIterator = mySelection.iterator;
					while(it.hasNext()){
						var shape:Shape = it.next();
						if(shape != null){
							if(shape.group !=null){
								if(referenceTable.indexOf(shape.group) != -1){
									Group(groupTable[referenceTable.indexOf(shape.group)]).add(shape);}
								else {
									var group:Group = new Group(drawfield);
									if(shape.group.myTranslation != null)
										group.myTranslation = shape.group.myTranslation.clone();
									if(shape.group.myRotation != null)
										group.myRotation = shape.group.myRotation.clone();
									group.add(shape);
									groupTable.push(group);
									referenceTable.push(shape.group);
								}
							}
						}
					}
				}
				for each(var g:Group in groupTable){
					degroupGroup(g);
				}
			}
		}
		
		public function degroupGroup(group:Group):void
		{
			if(group !=null){
				var translation:Translation = group.myTranslation;
				var rotation:Rotation = group.myRotation;
				
				var iter:GroupIterator = group.iterator;
				while(iter.hasNext())
				{
					var shape:Shape = iter.next();
					mySelection.add(shape);
					groupSelection(null);
					//a group is made for each shape in selection
					//now because a group consisting of only one shape
					// is immediatly dissolved, all shapes are degrouped
					shape.myTranslation = translation;
					shape.myRotation = rotation;
				}
			}
		}
		
		public function dragON(e:MouseEvent):void
		{
			drawfield.drawer = ShapeDrawerFactory.createDrawer("Null","Deplacement");
		}
		
		public function duplicateSelection(e:Event):void
		{
			reselectForms(1);

			if(mySelection.shapes.length > 1){
				var referenceTable:Array = new Array();
				var groupTable:Array = new Array();
				var ungroupedForms:Array = new Array();
				if(mySelection.containsOneElement() 
					&& Shape(mySelection.shapes[0]).group != null) //overkill? just leave it 
				{
					var copySelection:Group = Group(Shape(mySelection.shapes[0]).group.clone());
					copySelection.redraw();
					groupTable.push(copySelection);
				}
				else{
					var it:GroupIterator = mySelection.iterator;
					while(it.hasNext()){
						var shape:Shape = it.next();
						if(shape != null){
							var copy:Shape = Shape(shape.clone());
							copy.redraw();
							if(shape.group !=null){
								if(referenceTable.indexOf(shape.group) != -1){
									Group(groupTable[referenceTable.indexOf(shape.group)]).add(copy);}
								else {
									var group:Group = new Group(drawfield);
									group.add(copy);
									groupTable.push(group);
									referenceTable.push(shape.group);
									if(shape.group.myTranslation != null)
										group.myTranslation = shape.group.myTranslation.clone();
									if(shape.group.myRotation != null)
										group.myRotation = shape.group.myRotation.clone();
								}
							}
							else{
								ungroupedForms.push(copy);
							}
						}
					}
				}
				for each(var s:Shape in ungroupedForms)
					drawfield.addElement(s);
				for each(var g:Group in groupTable){
					drawfield.addElement(g);
				}
			}
			else if(mySelection.shapes.length == 1){
				Shape(mySelection.shapes[0]);
				var copyy:Shape = Shape(Shape(mySelection.shapes[0]).clone());
				copyy.redraw();
				drawfield.addElement(copyy);
			}
			
			codeRenderer.updateCode(drawfield.elements); 
			reselectForms(2);
			loadCode();
			
		}
		
		public function emptyMap(e:Event):void
		{
			drawfield.elements = new Array;
			codeRenderer.updateCode(drawfield.elements);
			loadCode();
			drawfield.drawer = ShapeDrawerFactory.createDrawer("Gomme");
		}
		
		private function keyUpLoadCode(e:KeyboardEvent):void
		{
			if(e.keyCode == Keyboard.ENTER){
				loadCode();
				codeRenderer.updateCode(drawfield.elements);
			}
			drawfield.drawer = ShapeDrawerFactory.createDrawer("Null","Selection");
		}
		
		public function updateCode(e:Event = null):void
		{
			codeRenderer.updateCode(drawfield.elements);
//			try{
//				Getter.communication.commitRevision(codeRenderer.code);
//			}
//			catch(e:Error)
//			{
//
//			}
		}
		
		private function updateHistory(e:Event):void
		{
			history.push(codeRenderer.code);
		}
		
		public function groupAll(event:Event):void
		{
			mySelection.empty();
			if(drawfield.elements.length > 1){
				var it:ElementsIterator = new ElementsIterator(drawfield.elements);
				while(it.hasNext())
				{
					var e:Element = it.next();
					if(e is Shape){
						mySelection.add(Shape(e));
					}
					else if(e is Group){
						for each (var s:Shape in Group(e).shapes)
							mySelection.add(s);
					}
				}
				groupSelection(event);
//				selectionCode.appendText(theGroup.getCode());
			}
			else if (drawfield.elements.length == 1){
				if(drawfield.elements[0] is Group)
				{
					var group:Group = drawfield.elements[0];
					degroupGroup(group);
				}
			}
		}

		public function deleteSelection(e:Event):void
		{
			var drawer:ShapeDrawer = drawfield.drawer;
			drawer.terminate();
			var selection:Selection = new Selection(null);
			selection.addGroup(mySelection);
			drawfield.drawer = ShapeDrawerFactory.createDrawer("Gomme");
			var it:GroupIterator = selection.iterator;
			while(it.hasNext()) 
				it.next().erase();
			if(drawer is SelectionRectangleDrawer)
				drawfield.drawer = drawer;
			else
				drawfield.drawer = ShapeDrawerFactory.createDrawer("Null","Selection");
		}
		
		public function turnON(e:MouseEvent):void
		{
			if(mySelection.containsOneElement()){
				drawfield.drawer = ShapeDrawerFactory.createDrawer("Tournage");
				drawfield.drawer.clickHandler(e);
			}
		}
		
		public function mouseWheelZoom(e:MouseEvent):void
		{			
			var m:Matrix = drawfield.transform.matrix;
			m.scale( 1+e.delta/Properties.zoomPrecision, 1+e.delta/Properties.zoomPrecision);
			m.translate(e.stageX*(-e.delta/Properties.zoomPrecision), e.stageY*(-e.delta/Properties.zoomPrecision));
			drawfield.transform.matrix = m;
		}
		
		public function loadPrevious(e:Event):void
		{
			loadCode(history.previous());
		}
		
		public function loadNext(e:Event):void
		{
			loadCode(history.next());
		}

		//UPDATE METHODS
		public function loadCode(code:String = null):void
		{
			reselectForms(1);
			
			var parser:CodeParser = codeRenderer.parser;
			var map:Array = parser.parseMap( code == null ? codeRenderer.code : code);
			drawfield.elements = map;
			codeRenderer.code = parser.read();
			history.push(parser.read());
			
			reselectForms(2);
		}

		private function updateCounter():void
		{
			drawingPanel.counter.text = ""+drawfield.shapeCounter+"/120";
		}
		
		private function showSelectionPanelOrNot():void
		{
			if(State.state == "Selection" && mySelection.containsOneElement())
			{		
				if(selectionPanel == null || (selectionPanel != null && selectionPanel.parent == null)){
					selectionPanel = new SelectionPanel(mySelection, 200, 408);
					addChildAt(selectionPanel,1);
					selectionPanel.addEventListener(KeyboardEvent.KEY_UP, updateCode);
				}
			}
			else try{
				if(selectionPanel != null)
					removeChild(selectionPanel);
			}catch(e:ArgumentError){
				
			}
		}
		
		public function showOptions(e:Event):void
		{
			addChild(options);
		}
		
		public function hideOptions(e:Event):void
		{
			if(contains(options))
				removeChild(options);
		}
		
		// GETTERS & SETTERS
		
		public function get drawfield():Drawboard
		{
			return _drawfield;
		}

		public function set drawfield(value:Drawboard):void
		{
			_drawfield = value;
		}

		public function set state(value:String):void
		{
			
			if(value != "Selection" && value != "Tournage")
			{
				mySelection.empty();
			}
			
			if(value != "Dessin"){
				drawingPanel.unselect();
			}
			
			if(value != "Perspective")
			{
				this.stopDrag();
			}

			State.state = value;
			
			var color:uint = State.color;
			drawfield.drawHitAreaRectangle(color);
		}

		public function get drawingPanel():DrawingPanel
		{
			return _drawingPanel;
		}

		public function set drawingPanel(value:DrawingPanel):void
		{
			_drawingPanel = value;
		}

		public function get mySelection():Selection
		{
			return _mySelection;
		}


	}
}